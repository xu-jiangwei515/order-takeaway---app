package com.example.sa.http;

import java.util.HashMap;
import java.util.Map;

import com.example.sa.http.Content;
import com.lidroid.xutils.http.RequestParams;

public class publicData {

	HashMap<String,String> map=new HashMap<String, String>();	
	static publicData pData=null;
	//单例map
	public static publicData getpublicData(){
		if (pData==null) {
			pData=new publicData();
		}
		return pData;
	}
	
	//map封装进RequestParams里
	public RequestParams getRequestParams(HashMap<String, String> map){
		RequestParams params = new RequestParams();
		if(null!=map){
			System.out.println("服务器地址及接口="+Content.path1+Content.UserInfo);
			for (Map.Entry<String, String> entry : map.entrySet()) {
				params.addBodyParameter(entry.getKey(),entry.getValue());
				System.out.println("参数= " + entry.getKey() + "     值= " + entry.getValue());
			}
		}
		return params;
	}
	//封装登陆数据
	public  RequestParams mapLogin(String LoginName,String userpwd){
		//只用一个map
		if (map.size()>0) {
			map.clear();
		}
		map.put("LoginName", LoginName);
		map.put("userpwd", userpwd);
		return getRequestParams(map);
	}
	//http://192.168.1.170:8002/UserInfo.aspx?UserID=1//用户信息
	public  RequestParams mapUserInfo(String UserID){
		//只用一个map
		if (map.size()>0) {
			map.clear();
		}
		map.put("UserID", UserID);
		return getRequestParams(map);
	}
}
