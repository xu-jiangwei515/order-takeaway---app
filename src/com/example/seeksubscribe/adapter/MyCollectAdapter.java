package com.example.seeksubscribe.adapter;

import java.util.ArrayList;

import com.example.entity.ShopLv;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.MycolletvActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class MyCollectAdapter extends BaseAdapter{

	private MycolletvActivity context;
	private LayoutInflater inflater;
	private ArrayList<ShopLv> list;
	private int mRightWidth;

	public MyCollectAdapter(MycolletvActivity context,ArrayList<ShopLv> list, int rightWidth){
		this.context=context;
		this.list=list;
		inflater=LayoutInflater.from(context);
		mRightWidth = rightWidth;
		
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		handerView hanger=null;
		if(convertView==null){
			convertView=inflater.inflate(R.layout.item_mycollect, null);
			hanger=new handerView();
			hanger.item_left = (LinearLayout) convertView.findViewById(R.id.item_left);
			hanger.item_right = (RelativeLayout) convertView.findViewById(R.id.item_right);
			hanger.shop_name_tv=(TextView) convertView.findViewById(R.id.shop_name_tv);
			hanger.shop_img_iv=(ImageView) convertView.findViewById(R.id.shop_img_iv);
			convertView.setTag(hanger);
		}else {
			hanger=(handerView) convertView.getTag();
		}
		ShopLv shopcollect = list.get(position);
		hanger.shop_name_tv.setText(shopcollect.getShopName());
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.restaurant_gali2);
		hanger.shop_img_iv.setImageBitmap(bitmap);
		LinearLayout.LayoutParams lp1 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		hanger.item_left.setLayoutParams(lp1);
		LinearLayout.LayoutParams lp2 = new LayoutParams(mRightWidth, LayoutParams.MATCH_PARENT);
		hanger.item_right.setLayoutParams(lp2);
		hanger.item_right.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener != null) {
					mListener.onRightItemClick(v, position);
				}
			}
		});
		return convertView;
	}
	class handerView{
		LinearLayout item_left;
		RelativeLayout item_right;
		TextView shop_name_tv;
		ImageView shop_img_iv;
	}
	private onRightItemClickListener mListener = null;

	public void setOnRightItemClickListener(onRightItemClickListener listener) {
		mListener = listener;
	}

	public interface onRightItemClickListener {
		void onRightItemClick(View v, int position);
	}

}
