package com.example.seeksubscribe.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.example.entity.Site;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.AddSiteActivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class MessageAdapter extends BaseAdapter{

	private Context context;
	private LayoutInflater inflater;
	private ArrayList<Map<String, String>> list;

	public MessageAdapter(Context context,ArrayList<Map<String, String>> list){
		this.context=context;
		this.list=list;
		inflater=LayoutInflater.from(context);
		
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		handerView hanger=null;
		if(convertView==null){
			convertView=inflater.inflate(R.layout.item_notice, null);
			hanger=new handerView();
			hanger.new_content=(TextView)convertView.findViewById(R.id.new_content);
			hanger.new_time=(TextView)convertView.findViewById(R.id.new_time);
			convertView.setTag(hanger);
		}else {
			hanger=(handerView) convertView.getTag();
		}
		 Map<String, String> map = list.get(position);
		hanger.new_content.setText(map.get("content"));
		hanger.new_time.setText(map.get("time"));
		return convertView;
	}
	class handerView{
		TextView new_content;
		TextView new_time;
	}
}
