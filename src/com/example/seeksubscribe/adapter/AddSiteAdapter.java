package com.example.seeksubscribe.adapter;

import java.util.ArrayList;

import com.example.entity.Site;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.AddSiteActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class AddSiteAdapter extends BaseAdapter{

	private AddSiteActivity context;
	private LayoutInflater inflater;
	private ArrayList<Site> list;
	private int mRightWidth;

	public AddSiteAdapter(AddSiteActivity context,ArrayList<Site> list,int rightWidth){
		this.context=context;
		this.list=list;
		inflater=LayoutInflater.from(context);
		mRightWidth = rightWidth;
		
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		handerView hanger=null;
		if(convertView==null){
			convertView=inflater.inflate(R.layout.item_expressage_site, null);
			hanger=new handerView();
			hanger.people_name_tv=(TextView) convertView.findViewById(R.id.people_name_tv);
			hanger.number_tel_tv=(TextView) convertView.findViewById(R.id.number_tel_tv);
			hanger.site_tv=(TextView) convertView.findViewById(R.id.site_tv);
			hanger.item_left = (LinearLayout) convertView.findViewById(R.id.item_left);
			hanger.item_right = (RelativeLayout) convertView.findViewById(R.id.item_right);
			convertView.setTag(hanger);
		}else {
			hanger=(handerView) convertView.getTag();
		}
		Site Site = list.get(position);
		hanger.people_name_tv.setText(Site.getSitePeopleName());
		hanger.number_tel_tv.setText(Site.getSiteTel());
		hanger.site_tv.setText(Site.getSite());
		LinearLayout.LayoutParams lp1 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		hanger.item_left.setLayoutParams(lp1);
		LinearLayout.LayoutParams lp2 = new LayoutParams(mRightWidth, LayoutParams.MATCH_PARENT);
		hanger.item_right.setLayoutParams(lp2);
		hanger.item_right.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener != null) {
					mListener.onRightItemClick(v, position);
				}
			}
		});
		return convertView;
	}
	class handerView{
		TextView people_name_tv;
		TextView number_tel_tv;
		TextView site_tv;
		LinearLayout item_left;
		RelativeLayout item_right;
	}
	private onRightItemClickListener mListener = null;

	public void setOnRightItemClickListener(onRightItemClickListener listener) {
		mListener = listener;
	}

	public interface onRightItemClickListener {
		void onRightItemClick(View v, int position);
	}
}
