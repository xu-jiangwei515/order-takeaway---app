package com.example.seeksubscribe.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.seeksubscribe.R;

public class ActivityServerCenter extends BaseActivity{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private LinearLayout versions_ll;
	private LinearLayout cooperation_ll;
	private LinearLayout copyright_ll;
	private LinearLayout contact_me_ll;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_me_set);
		findviewByid();
		intitData();
		init();
		initlistenter();
	}

	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//招商合作
		cooperation_ll=(LinearLayout)findViewById(R.id.cooperation_ll);
		//版权声明
		copyright_ll=(LinearLayout)findViewById(R.id.copyright_ll);
		//版本信息
		versions_ll=(LinearLayout)findViewById(R.id.versions_ll);
		//联系我们
		contact_me_ll=(LinearLayout)findViewById(R.id.contact_me_ll);
		
	}
	private void intitData() {
		// TODO Auto-generated method stub
		
	}


	private void init() {
		title_location_tv.setText("客服中心");
		//从右往左第1个图标（收藏）
		title_reight_iv.setVisibility(View.GONE);
		//从右往左第2个图标（分享）
		title_toreight_iv.setVisibility(View.GONE);
		
	}

	private void initlistenter() {
		title_return_rl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
		cooperation_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(ActivityServerCenter.this,ActivityCooperation.class);
				startActivity(intent);
			}
		});
		copyright_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(ActivityServerCenter.this,CopyrightActivity.class);
				startActivity(intent);
			}
		});
		versions_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(ActivityServerCenter.this,VersionsActivity.class);
				startActivity(intent);
			}
		});
		contact_me_ll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(ActivityServerCenter.this,ContactActivity.class);
				startActivity(intent);
				
			}
		});
	}
}
