package com.example.seeksubscribe.Activity;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.example.fragment.OrderFragment;
import com.example.fragment.HomePageFragment;
import com.example.fragment.MeFragment;
import com.example.seeksubscribe.R;

public class HomeActivity extends FragmentActivity implements OnClickListener{
	private final RadioButton[] radioButton=new RadioButton[5];
	private RadioGroup home_radio_button_group;
	private ViewPager viewpager;
	private ArrayList<Fragment> fragmentList;
	private HomePageFragment homepageFragment;
	private OrderFragment orderfragment;
	private MeFragment mefragment;
	private long touchtime = 0;
	private PopupWindow window=null;
	@Override
	protected void onCreate(Bundle arg0) {
		requestWindowFeature(1);
		super.onCreate(arg0);
		setContentView(R.layout.activity_home);
		findViewByid();
		initView();
	}
	private void initView() {
		fragmentList=new ArrayList<Fragment>();
		homepageFragment=new HomePageFragment(this);
		orderfragment=new OrderFragment(this);
		mefragment=new MeFragment(this);

		fragmentList.add(homepageFragment);
		fragmentList.add(orderfragment);
		fragmentList.add(mefragment);
		viewpager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {

			@Override
			public int getCount() {
				return fragmentList.size();
			}

			@Override
			public Fragment getItem(int arg0) {
				return fragmentList.get(arg0);
			}
		});
		viewpager.setCurrentItem(0);
		viewpager.setOnPageChangeListener(new MyOnPageChangeListener());
		home_radio_button_group.setOnCheckedChangeListener(listener);
	}

	private void findViewByid() {
		radioButton[0]=(RadioButton)findViewById(R.id.home_page);
		radioButton[1]=(RadioButton)findViewById(R.id.order_rb);
		radioButton[2]=(RadioButton)findViewById(R.id.me_rb);
		home_radio_button_group=(RadioGroup)findViewById(R.id.home_radio_button_group);
		viewpager=(ViewPager)findViewById(R.id.viewpager);
	}
	RadioGroup.OnCheckedChangeListener listener=new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			Log.v("onCheckedChanged", checkedId + "");
			switch (checkedId) {
			case R.id.home_page:
				viewpager.setCurrentItem(0);
				break;
			case R.id.order_rb:
				viewpager.setCurrentItem(1);
				break;
			case R.id.me_rb:
				viewpager.setCurrentItem(2);
				break;
			}
		}
	};
	public class MyOnPageChangeListener implements OnPageChangeListener{

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			Log.v("onPageScrollStateChanged", arg0 + "");
		}

		@Override
		public void onPageSelected(int arg0) {
			Log.v("onPageSelected", arg0 + "");
			radioButton[arg0].setChecked(true);
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			long currTime = System.currentTimeMillis();

			if ((currTime - touchtime) > 2000) {
				Toast.makeText(HomeActivity.this, "再点一次退出找预订",
						Toast.LENGTH_SHORT).show();
				touchtime = currTime;
			} else {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				this.startActivity(intent);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);  
				finish();
				System.exit(0);
			}
			return true;
		}
		return false;
	}
	@Override
	public void onClick(View arg0) {

	}

}
