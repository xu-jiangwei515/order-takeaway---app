package com.example.seeksubscribe.Activity;


import com.example.http.NetworkErrorLog;
import com.example.http.PoCRequestManager;
import com.example.http.PoCRequestManager.OnRequestFinishedListener;
import com.example.http.PoCService;
import com.example.seeksubscribe.R;
import com.example.utils.ToastUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

public  class BaseActivity extends Activity implements OnRequestFinishedListener{

	private PoCRequestManager mRequestManager;
	protected ProgressDialog waitDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// 直接在BaseActivity中通过单利模式获得PoCRequestManager的对象
		mRequestManager = PoCRequestManager.from(this);
		mRequestManager.addOnRequestFinishedListener(this);
		System.out.println("baseActivity运行");
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		dismissDialog();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mRequestManager.removeOnRequestFinishedListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mRequestManager.addOnRequestFinishedListener(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mRequestManager.removeOnRequestFinishedListener(this);
	}


	/**
	 * 启动全局等待对话框,请稍后
	 */
	public void showWaitDialog() {
		try {
			if (waitDialog == null) {
				waitDialog = new ProgressDialog(BaseActivity.this);
				waitDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				waitDialog.setCanceledOnTouchOutside(false);
			}
			ImageView view = new ImageView(this);
			view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));
			Animation loadAnimation = AnimationUtils.loadAnimation(BaseActivity.this,
					R.anim.rotate);
			view.startAnimation(loadAnimation);
			loadAnimation.start();
			view.setImageResource(R.drawable.loading);
			// waitDialog.setCancelable(false);
			if (!waitDialog.isShowing()) {
				waitDialog.show();
				waitDialog.setContentView(view);
			}
			Log.i("waitDialong.......", "waitDialong.......");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 关闭全局等待对话框,请稍后
	 */
	public void dismissDialog() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (waitDialog != null) {
					if (waitDialog.isShowing()) {
						waitDialog.dismiss();
					}
					waitDialog = null;
				}
			}
		});

	}
	// 访问服务器完成后调用
	@Override
	public void onRequestFinished(int requestId, int resultCode, Bundle payload) {
		// 成功后交由子类实现
				dismissDialog();
				if (resultCode == PoCService.ERROR_CODE) {
					if (payload != null) {
						final int errorType = payload.getInt(
								PoCRequestManager.RECEIVER_EXTRA_ERROR_TYPE, -1);
						NetworkErrorLog.networkErrorOperate(
								this.getApplicationContext(), errorType);
					} else {
						ToastUtil.makeShortText(this, "服务器出错！");
					}
				}
	}
	public void toast(String text,Context context){
		if (text.length()>0&&text!=null&&context!=null) {
			Toast.makeText(context, text, 1).show();
		}else {
			Log.i("toastc传值错误", "text="+text+"   context="+context.toString());
		}
	}
	@Override
	public void onRequestPrepareListener() {
		
	}
		
}
