package com.example.seeksubscribe.Activity;

import com.example.seeksubscribe.R;
import com.example.utils.ToastUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NewPasswordActivity extends BaseActivity implements OnClickListener{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private Button next_to_bt;
	private EditText password_et;
	private EditText password_to_et;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_password);
		findviewByid();
		intitData();
		init();
		initlistenter();
	}
	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//新密码
		password_et=(EditText)findViewById(R.id.password_et);
		//再次输入密码
		password_to_et=(EditText)findViewById(R.id.password_to_et);
		//完成
		next_to_bt=(Button)findViewById(R.id.next_to_bt);

	}

	private void intitData() {
		

	}

	private void init() {
		title_location_tv.setText("修改密码");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);

	}

	private void initlistenter() {
		title_return_rl.setOnClickListener(this);
		next_to_bt.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.next_to_bt:
			ToastUtil.makeLongText(NewPasswordActivity.this, "修改成功");
			finish();
			break;

		}
		
	}
	
}
