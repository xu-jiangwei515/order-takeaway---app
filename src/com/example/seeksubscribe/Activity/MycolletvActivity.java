package com.example.seeksubscribe.Activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dao.MyApplication;
import com.example.eat.activity.EatSeekShopActivity;
import com.example.entity.ShopLv;
import com.example.myView.SwipeListView;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.adapter.MyCollectAdapter;
import com.example.seeksubscribe.adapter.MyCollectAdapter.onRightItemClickListener;



public class MycolletvActivity extends BaseActivity implements OnClickListener{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private SwipeListView mycollect_lv;
	private ArrayList<ShopLv> shoplist=new ArrayList<ShopLv>();
	private MyCollectAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mycollect);
		findviewByid();
		intitData();
		init();
		listenter();
		initlistenter();
	}


	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//收藏列表
		mycollect_lv=(SwipeListView)findViewById(R.id.mycollect_lv);
	}

	private void intitData() {
		//通过网络获取
		for(int i=0;i<10;i++){
			ShopLv shoplv = new ShopLv();
			shoplv.setShopId(i+"");
			shoplv.setShopName("咖喱大叔");
			shoplv.setPathPicture("图片URL地址");
			shoplist.add(shoplv);
		}
	}

	private void init() {
		title_location_tv.setText("我的收藏");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);
		adapter = new MyCollectAdapter(MycolletvActivity.this, shoplist,mycollect_lv.getRightViewWidth());
		mycollect_lv.setAdapter(adapter);
	}

	private void listenter() {
		title_return_rl.setOnClickListener(this);
		mycollect_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {


				MyApplication ma = MyApplication.getInstance();
				//重新选择商店清下总价和菜品集合
				ma.sun_money="0";
				//每次点击初始化
				ma.disherList.clear();
				//标记重新获取数据
				ma.mark=2;
				String shopID = shoplist.get(position).getShopId();
				Intent i = new Intent(MycolletvActivity.this, EatSeekShopActivity.class);
				Bundle shopbundle = new Bundle();
				shopbundle.putInt("shopld",Integer.parseInt(shopID));
				i.putExtra("shopbundle", shopbundle);
				startActivity(i);
			}
		});
	}
	private void initlistenter() {
		adapter.setOnRightItemClickListener(new onRightItemClickListener() {

			@Override
			public void onRightItemClick(View v, final int position) {
				//删除后掉服务器接口，重新获取下数据
				intitData();
				shoplist.remove(position);
				adapter = new MyCollectAdapter(MycolletvActivity.this, shoplist,mycollect_lv.getRightViewWidth());
				mycollect_lv.setAdapter(adapter);
				initlistenter();
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		}

	}
}
