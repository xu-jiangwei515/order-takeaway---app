package com.example.seeksubscribe.Activity;

import com.example.seeksubscribe.R;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActivityCooperation extends BaseActivity{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private EditText shop_name_et;
	private EditText shop_host_name_et;
	private EditText shop_host_phone_et;
	private EditText shop_message_et;
	private Button cooperation_ok_bt;
	private TextView me_phone_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cooperation);
		findViewById();
		init();
		listener();
	}

	private void findViewById() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//店名称
		shop_name_et=(EditText)findViewById(R.id.shop_name_et);
		//店主姓名
		shop_host_name_et=(EditText)findViewById(R.id.shop_host_name_et);
		//联系电话
		shop_host_phone_et=(EditText)findViewById(R.id.shop_host_phone_et);
		//经营类型
		shop_message_et=(EditText)findViewById(R.id.shop_message_et);
		//提交按钮
		cooperation_ok_bt=(Button)findViewById(R.id.cooperation_ok_bt);
		//联系我们的电话
		me_phone_tv=(TextView)findViewById(R.id.me_phone_tv);
	}

	private void init() {
		title_location_tv.setText("招商合作");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);

	}

	private void listener() {
		title_return_rl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		me_phone_tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String phone = me_phone_tv.getText().toString();
				 //用intent启动拨打电话  
                Intent intent = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+phone));  
                startActivity(intent);  
			}
		});
		cooperation_ok_bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String shopNameString=shop_name_et.getText().toString();
				String shopHostNameString=shop_host_name_et.getText().toString();
				String shopHostPhoneString=shop_host_phone_et.getText().toString();
				String shopMessageString=shop_message_et.getText().toString();
				if (null!=shopNameString&&shopNameString.length()>0) {
					toast("请填写店铺名称", ActivityCooperation.this);
					return;
				}
				if (null!=shopHostNameString&&shopHostNameString.length()>0) {
					toast("请填写店主姓名", ActivityCooperation.this);
					return;
				}
				if (null!=shopHostPhoneString&&shopHostPhoneString.length()>0) {
					toast("请填写店主手机", ActivityCooperation.this);
					return;
				}
				if (null!=shopMessageString&&shopMessageString.length()>0) {
					toast("请填写经营类型", ActivityCooperation.this);
					return;
				}
				//提交服务器
				finish();
			}
		});

	}
}
