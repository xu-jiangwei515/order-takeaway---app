package com.example.seeksubscribe.Activity;



import com.example.seeksubscribe.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Window;

public class StartActivity extends BaseActivity {
	private int mark=0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_start);
		new startTask().execute();
	}

	class startTask extends AsyncTask<String, Void, String> {


		@Override
		protected String doInBackground(String... params) {
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			SharedPreferences sf = StartActivity.this.getSharedPreferences("success_file_store", Context.MODE_WORLD_READABLE);
			boolean flag = sf.getBoolean("flag", false);
			if (!flag) {
				Editor editor = sf.edit();
				editor.putBoolean("flag", true);
				editor.commit();
				Intent in = new Intent(StartActivity.this, GuideActivity.class);
				StartActivity.this.startActivity(in);
			} else {
				Intent in = new Intent(StartActivity.this, HomeActivity.class);
				in.putExtra("userLogin", "start");
				StartActivity.this.startActivity(in);
			}
			finish();
			super.onPostExecute(result);
		}

	}


}
