package com.example.seeksubscribe.Activity;

import java.util.ArrayList;
import java.util.List;

import com.example.seeksubscribe.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GuideActivity extends BaseActivity {

	ViewPager viewpager;
	List<View> list = new ArrayList<View>();
	private View view1, view2, view3,view4,view5;
	int width;
	private Button btn_jinru;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guide);
		setView();

		DisplayMetrics metric = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metric);
		width = metric.widthPixels; 
		Myadapter adapter = new Myadapter();
		viewpager.setAdapter(adapter);
	}

	public void setView() {

		viewpager = (ViewPager) this.findViewById(R.id.viewpager);
		view1 = View.inflate(this, R.layout.item_guide1, null);
		view2 = View.inflate(this, R.layout.item_guide2, null);
		view3 = View.inflate(this, R.layout.item_guide3, null);
		view4 = View.inflate(this, R.layout.item_guide4, null);
		view5 = View.inflate(this, R.layout.item_guide5, null);
		list.add(view1);
		list.add(view2);
		list.add(view3);
		list.add(view4);
		list.add(view5);
		btn_jinru = (Button) view5.findViewById(R.id.btn_jinru);
		btn_jinru.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				start();
			}
		});
	}

	public class Myadapter extends PagerAdapter {

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			((ViewPager) container).removeView(list.get(position));
		}

		@Override
		public Object instantiateItem(View container, int position) {
			((ViewPager) container).addView(list.get(position));
			return list.get(position);
		}
	}

	public void start() {
		Intent in = new Intent(GuideActivity.this, HomeActivity.class);
		GuideActivity.this.startActivity(in);
		GuideActivity.this.finish();
	}


}
