package com.example.seeksubscribe.Activity;

import com.example.seeksubscribe.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ContactActivity extends BaseActivity implements OnClickListener{
	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private TextView phone_tv;
	private TextView conntct_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact);
		findviewByid();
		intitData();
		init();
		initlistenter();
	}
	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//电话
		phone_tv=(TextView)findViewById(R.id.phone_tv);
		//网址
		conntct_tv=(TextView)findViewById(R.id.conntct_tv);
	}

	private void intitData() {
		conntct_tv.setText(Html.fromHtml("<a href='http://www.baidu.com'>www.baidu.com</a>"));
		conntct_tv.setMovementMethod(LinkMovementMethod.getInstance());
	}

	private void init() {
		title_location_tv.setText("联系我们");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);

	}

	private void initlistenter() {
		title_return_rl.setOnClickListener(this);
		phone_tv.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.phone_tv:
			String phoneString=phone_tv.getText().toString();
			if (null!=phoneString&&phoneString.length()>0) {
				//用intent启动拨打电话  
				Intent intent = new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+phoneString));  
				startActivity(intent);  
			}
			break;
		}
		
	}
	
}
