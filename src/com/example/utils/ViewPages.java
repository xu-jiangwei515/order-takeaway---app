package com.example.utils;

import java.util.ArrayList;
import java.util.List;

import com.example.seeksubscribe.R;


import android.app.Activity;
import android.graphics.Color;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/** 滑动分布工具 */
public class ViewPages {
	private ViewPager mPager;// 页卡内容
	private List<View> mListViews; // Tab页面列表
	private ImageView cursor;// 动画图片
	/** 标题组 */
	private List<TextView> titleViews;
	private int moveSize = 0;// 动画图片偏移量
	private int currIndex = 0;// 当前页卡编号
	int currentItem;
	Activity activity;
	/** 分布容器 */
	View pagecon;

	/**
	 * @param activity
	 *            上下文
	 * @param pagecon
	 *            分页容器
	 * @param listViews
	 *            分页子页组
	 * @param currentItem
	 *            选中的页下标
	 */
	public ViewPages(Activity activity, View pagecon, List<View> listViews, int currentItem) {
		titleViews = new ArrayList<TextView>();
		this.activity = activity;
		this.pagecon = pagecon;
		this.mListViews = listViews;
		this.currentItem = currentItem;
		InitTextView();
		InitImageView();
		InitViewPager();
	}

	/**
	 * 初始化头标
	 */
	private void InitTextView() {
		//不居中包含textView控件作为滑动头部，获取头部加入集合中
		LinearLayout titles = (LinearLayout) pagecon.findViewById(R.id.title_LinearLayout);
		for (int i = 0; i < titles.getChildCount(); i++) {
			TextView tv = (TextView) titles.getChildAt(i);
			tv.setOnClickListener(new MyOnClickListener(i));
			titleViews.add(tv);
		}
	}

	/**
	 * 头标点击监听
	 */
	public class MyOnClickListener implements View.OnClickListener {
		private int index = 0;

		public MyOnClickListener(int i) {
			index = i;
		}

		@Override
		public void onClick(View v) {
			mPager.setCurrentItem(index);
		}
	};

	public void setCurrentItem(int i) {
		mPager.setCurrentItem(i);
	}

	/**
	 * 初始化ViewPager
	 */
	private void InitViewPager() {
		mPager = (ViewPager) pagecon.findViewById(R.id.vPager);
		mPager.setAdapter(new MyPagerAdapter(mListViews));
		mPager.setOnPageChangeListener(new MyOnPageChangeListener());
		titleViews.get(0).setTextColor(Color.rgb(194, 74, 76));
		mPager.setCurrentItem(currentItem);
	}

	/**
	 * ViewPager适配器
	 */
	public class MyPagerAdapter extends PagerAdapter {
		public List<View> mListViews;

		public MyPagerAdapter(List<View> mListViews) {
			this.mListViews = mListViews;
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(mListViews.get(arg1));
		}

		@Override
		public void finishUpdate(View arg0) {
		}

		@Override
		public int getCount() {
			return mListViews.size();
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(mListViews.get(arg1), 0);
			return mListViews.get(arg1);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == (arg1);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
		}
	}

	/**
	 * 初始化动画
	 */
	@SuppressWarnings("deprecation")
	private void InitImageView() {
		cursor = (ImageView) pagecon.findViewById(R.id.cursor);
		moveSize = 
				activity.
				getWindowManager().getDefaultDisplay().getWidth() / 
				titleViews.size();
		LayoutParams params = cursor.getLayoutParams();
		params.width = moveSize - titleViews.size() + 1;
		cursor.setLayoutParams(params);
	}

	/**
	 * 页卡切换监听
	 */
	public class MyOnPageChangeListener implements OnPageChangeListener {
		
		@Override
		public void onPageSelected(int i) {
			Animation animation = null;

			for (int j = 0; j < titleViews.size(); j++) {
				if (i == j) {
					titleViews.get(j).setTextColor(Color.rgb(194, 74, 76));
				} else {
					titleViews.get(j).setTextColor(Color.rgb(47, 47, 47));
				}
			}
			animation = new TranslateAnimation(currIndex * moveSize, i * moveSize, 0, 0);
			currIndex = i;
			animation.setFillAfter(true);// True:图片停在动画结束位置
			animation.setDuration(300);
			cursor.startAnimation(animation);
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	}
}

