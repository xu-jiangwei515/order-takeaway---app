package com.example.utils;

import android.os.Environment;

public class ConstantsUtil {
	/** 用户名输入框输入字符长度 **/
	public static final int LEN_USERNAME = 20;
	/** 手机号输入框输入字符长度 **/
	public static final int LEN_PHONE = 11;
	/** 密码输入框输入字符长�? **/
	public static final int LEN_PWD = 15;
	/** 验证码输入框输入字符长度 **/
	public static final int LEN_CODE = 6;
	public static final String SDCARD_PATH = Environment
			.getExternalStorageDirectory().getPath() + "/talkart/";
	/** 是否登陆 **/
	public static String IS_LOGIN = "isLogin";
	/** 会话ID **/
	public static String SESSION_ID = "sessionId";

}
