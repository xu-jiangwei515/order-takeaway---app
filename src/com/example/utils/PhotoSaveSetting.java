package com.example.utils;



import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PhotoSaveSetting {
	public static void setPhotoSetting(Context context, boolean isopen) {
		SharedPreferences sp = context.getSharedPreferences("save_photo_sp",
				context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putBoolean("save_photo", isopen);
		editor.commit();
	}

	public static boolean getPhotoSetting(Context context) {
		SharedPreferences sp = context.getSharedPreferences("save_photo_sp",
				context.MODE_PRIVATE);
		return sp.getBoolean("save_photo", true);
	}

}
