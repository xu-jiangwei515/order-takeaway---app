package com.example.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import android.content.Context;
import android.os.Bundle;

/**
 * MD5生成�?
 * 
 * @author zhang.zk
 * 
 */
public class MD5Util {
	/**
	 * 对参数进行MD5加密.方法内已对参数做加app_secret处理.
	 * 
	 * @param params
	 * @return
	 */
	public static String getMD5(Context context,
			List<Map<String, String>> params) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < params.size(); i++) {
			Map<String, String> map = params.get(i);
			String key = map.get("key");
			String value = map.get("value");
			if (!StringUtil.isEmpty(value)) {
				buffer.append(key);
				buffer.append(value);
			}
		}
		buffer.append(Utils.getAppSecret(context));

		return toMD5(buffer.toString());
	}

	/**
	 * 对参数进行MD5加密.方法内已对参数做加app_secret处理.
	 * 
	 * @param bundle
	 * @return
	 */
	public static String getMD5(Context context, Bundle bundle) {
		StringBuffer buffer = new StringBuffer();
		Set<String> keySet = bundle.keySet();
		List<String> list = new ArrayList<String>(keySet);
		Collections.sort(list);
		for (String key : list) {
			String value = bundle.getString(key);
			if (!StringUtil.isEmpty(value)) {
				buffer.append(key);
				buffer.append(value);
			}
		}
		buffer.append(Utils.getAppSecret(context));
		return toMD5(buffer.toString());
	}

	/**
	 * 对参数进行MD5加密.方法内已对参数做前后加app_secret处理.
	 * 
	 * @param string
	 * @return
	 */
	public static String getMD5(Context context, String string) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(string);
		buffer.append(Utils.getAppSecret(context));
		return toMD5(buffer.toString());
	}

	/**
	 * MD5加密.
	 * 
	 * @return
	 */
	public static String getMD5Digest(String content) {
		return toMD5(content);
	}

	/**
	 * MD5加密
	 * 
	 * @param string
	 * @return
	 */
	private static String toMD5(String string) {
		LogUtil.i("before MD5 = " + string);
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.reset();
			md.update(string.getBytes());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] digest = md.digest();
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < digest.length; i++) {
			String hexString = Integer.toHexString(digest[i] & 0xFF);
			if (hexString.length() == 1) {
				buffer.append("0").append(hexString);
			} else {
				buffer.append(hexString);
			}
		}
		String afterMD5 = buffer.toString().toUpperCase();
		LogUtil.i("after MD5 = " + afterMD5);
		return afterMD5;
	}

}
