package com.example.utils;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;

public class Rotate3dAnimation extends Animation {
	private float mFromDegrees;
	private float mToDegrees;
	private float mCenterX;
	private float mCenterY;
	private float mDepthZ;
	private boolean mReverse;
	private Camera mCamera;
	private ViewGroup group;
	public static Rotate3dAnimation rotation;
	private LinearInterpolator interpolator;
	private DisplayNextView nextView;
	private View view;

	public static Rotate3dAnimation createInstance() {
		if (rotation == null) {
			rotation = new Rotate3dAnimation();
		}
		return rotation;
	}

	/** 重置参数 */
	public void resetRotate3dAnimation(float fromDegrees, float toDegrees, float centerX, float centerY,
			float depthZ, boolean reverse) {
		mFromDegrees = fromDegrees;
		mToDegrees = toDegrees;
		mCenterX = centerX;
		mCenterY = centerY;
		mDepthZ = depthZ;
		mReverse = reverse;
	}

	@Override
	public void initialize(int width, int height, int parentWidth, int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
		mCamera = new Camera();
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		final float fromDegrees = mFromDegrees;
		float degrees = fromDegrees + ((mToDegrees - fromDegrees) * interpolatedTime);

		final float centerX = mCenterX;
		final float centerY = mCenterY;
		final Camera camera = mCamera;

		final Matrix matrix = t.getMatrix();

		camera.save();
		if (mReverse) {
			camera.translate(0.0f, 0.0f, mDepthZ * interpolatedTime);
		} else {
			camera.translate(0.0f, 0.0f, mDepthZ * (1.0f - interpolatedTime));
		}
		camera.rotateY(degrees);
		camera.getMatrix(matrix);
		camera.restore();

		matrix.preTranslate(-centerX, -centerY);
		matrix.postTranslate(centerX, centerY);
	}

	private final class DisplayNextView implements Animation.AnimationListener {
		public void onAnimationStart(Animation animation) {
		}

		public void onAnimationEnd(Animation animation) {
			view.post(new SwapViews());
		}

		public void onAnimationRepeat(Animation animation) {
		}
	}

	private final class SwapViews implements Runnable {
		public void run() {
			final float centerX = group.getWidth() / 2.0f;
			final float centerY = group.getHeight() / 2.0f;
			resetRotate3dAnimation(359, 0, centerX, centerY, 0.0f, false);
			if (mReverse) {
				view.startAnimation(rotation);
			}
		}
	}

	public void applyRotation(ViewGroup group, View view, int position, float start, float end) {
		interpolator = new LinearInterpolator();
		nextView = new DisplayNextView();
		this.group = group;
		this.view = view;
		final float centerX = group.getWidth() / 2.0f;
		final float centerY = group.getHeight() / 2.0f;

		resetRotate3dAnimation(start, end, centerX, centerY, 0.0f, true);
		rotation.setDuration(800);
		rotation.setFillAfter(true);
		rotation.setInterpolator(interpolator);
		rotation.setAnimationListener(nextView);
		view.startAnimation(rotation);
	}

	public void topVbottom(View view, float fromXDelta, float toXDelta, float fromYDelta, float toYDelta) {
		// 摇摆
		TranslateAnimation alphaAnimation = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta, toYDelta);
		alphaAnimation.setDuration(500);
		alphaAnimation.setRepeatCount(1);
		alphaAnimation.setRepeatMode(Animation.REVERSE);
		view.setAnimation(alphaAnimation);
		alphaAnimation.start();
	}
}
