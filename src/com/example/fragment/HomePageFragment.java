package com.example.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;
import com.example.dao.MyApplication;
import com.example.eat.activity.EatEaterySeekActivity;
import com.example.eat.activity.EatSeekActivity;
import com.example.entity.AddressLocation;
import com.example.myView.DetailGallery;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.CaptureActivity;
import com.example.seeksubscribe.Activity.HomeActivity;
import com.example.utils.ToastUtil;
import com.example.utils.Tool;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView.ScaleType;

public class HomePageFragment extends BaseFragment implements OnClickListener,AMapLocationListener{
	private View view;
	//定位按钮布局
	private RelativeLayout location_tl;
	//定位城市
	private TextView city_name_tv;
	//城市区域名称
	private String district="";
	//搜索iv
	private ImageView search_iv;
	//二维码
	private ImageView erweima_iv;
	//找吃
	private RelativeLayout home_eat_rl;
	//找玩
	private RelativeLayout home_fun_rl;
	//找唱
	private RelativeLayout home_sing_rl;
	//找吃店家推荐12345号
	private LinearLayout hone_eat_recommend1;
	private RelativeLayout hone_eat_recommend2;
	private RelativeLayout hone_eat_recommend3;
	private RelativeLayout hone_eat_recommend4;
	private RelativeLayout hone_eat_recommend5;
	//找玩店家推荐12345号
	private LinearLayout hone_fun_recommend1;
	private RelativeLayout hone_fun_recommend2;
	private RelativeLayout hone_fun_recommend3;
	private RelativeLayout hone_fun_recommend4;
	private RelativeLayout hone_fun_recommend5;



	//找唱店家推荐12345号
	private LinearLayout hone_sing_recommend1;
	private RelativeLayout hone_sing_recommend2;
	private RelativeLayout hone_sing_recommend3;
	private RelativeLayout hone_sing_recommend4;
	private RelativeLayout hone_sing_recommend5;

	//找吃店家推荐名字12345
	private TextView eat_shop_name_tv1;
	private TextView eat_shop_name_tv2;
	private TextView eat_shop_name_tv3;
	private TextView eat_shop_name_tv4;
	private TextView eat_shop_name_tv5;
	//找玩店家推荐名字12345
	private TextView fun_shop_name_tv1;
	private TextView fun_shop_name_tv2;
	private TextView fun_shop_name_tv3;
	private TextView fun_shop_name_tv4;
	private TextView fun_shop_name_tv5;
	//找唱店家推荐名字12345
	private TextView sing_shop_name_tv1;
	private TextView sing_shop_name_tv2;
	private TextView sing_shop_name_tv3;
	private TextView sing_shop_name_tv4;
	private TextView sing_shop_name_tv5;

	//找吃店家推荐s说明12345
	private TextView eat_shop_recommend_tv1;
	private TextView eat_shop_recommend_tv2;
	private TextView eat_shop_recommend_tv3;
	private TextView eat_shop_recommend_tv4;
	private TextView eat_shop_recommend_tv5;
	//找玩店家推荐说明12345
	private TextView fun_shop_recommend_tv1;
	private TextView fun_shop_recommend_tv2;
	private TextView fun_shop_recommend_tv3;
	private TextView fun_shop_recommend_tv4;
	private TextView fun_shop_recommend_tv5;
	//找唱店家推荐说明12345
	private TextView sing_shop_recommend_tv1;
	private TextView sing_shop_recommend_tv2;
	private TextView sing_shop_recommend_tv3;
	private TextView sing_shop_recommend_tv4;
	private TextView sing_shop_recommend_tv5;

	//找吃店家推荐头像12345
	private ImageView eat_shop_iv1;
	private ImageView eat_shop_iv2;
	private ImageView eat_shop_iv3;
	private ImageView eat_shop_iv4;
	private ImageView eat_shop_iv5;
	//找玩店家推荐头像12345
	private ImageView fun_shop_iv1;
	private ImageView fun_shop_iv2;
	private ImageView fun_shop_iv3;
	private ImageView fun_shop_iv4;
	private ImageView fun_shop_iv5;
	//找唱店家推荐头像12345
	private ImageView sing_shop_iv1;
	private ImageView sing_shop_iv2;
	private ImageView sing_shop_iv3;
	private ImageView sing_shop_iv4;
	private ImageView sing_shop_iv5;

	//每类大店家多出来的地址（吃，玩，唱）
	private TextView eat_shop_address_tv1;
	private TextView fun_shop_address_tv1;
	private TextView sing_shop_address_tv1;
	//当前上下文
	private HomeActivity homeActibity;
	//---------------------幻灯片-----------------------------
	private DetailGallery myGallery;
	private RadioGroup gallery_points;
	private RadioButton[] gallery_point;
	private LayoutInflater inflater;
	private LinearLayout layout;
	//-----------------------地图定位--------------------
	private LocationManagerProxy mLocationManagerProxy;
	private int mark=1;
	//定位未成功显示的界面
	private ImageView dingwei_loging_iv;
	private TextView dingwei_explain_tv;
	private Button angew_dingwei_bt;
	//定位成功前页面
	private LinearLayout dingwei_no_ll;
	//定位成功后页面
	private LinearLayout dingwei_ok_ll;
	//定位标记1为成功
	private int ditumark;

	public HomePageFragment(HomeActivity context) {
		this.homeActibity=context;
	}
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		view=inflater.inflate(R.layout.fragment_home_page, null);
		this.inflater=inflater;
		findviewByid();
		city_name_tv.setText(district);
		//定位  只运行一次
		if(mark==1){
			dingwei();
			mark=2;
		}
		init();
		addEvn();
		autogallery();
		initListener();
		if (ditumark==1) {
			dingwei_no_ll.setVisibility(View.GONE);
			dingwei_ok_ll.setVisibility(View.VISIBLE);
			angew_dingwei_bt.setVisibility(View.GONE);
			dingwei_explain_tv.setVisibility(View.GONE);
		}else if(ditumark==2){
			dingwei_no_ll.setVisibility(View.VISIBLE);
			dingwei_ok_ll.setVisibility(View.GONE);
			angew_dingwei_bt.setVisibility(View.VISIBLE);
			dingwei_explain_tv.setVisibility(View.GONE);
		}else {
			dingwei_no_ll.setVisibility(View.VISIBLE);
			dingwei_ok_ll.setVisibility(View.GONE);
			angew_dingwei_bt.setVisibility(View.GONE);
			dingwei_explain_tv.setVisibility(View.VISIBLE);
		}
		
		return view;
	}
	
	
	
	private void findviewByid() {
		
		
		dingwei_no_ll=(LinearLayout)view.findViewById(R.id.dingwei_no_ll);
		dingwei_ok_ll=(LinearLayout)view.findViewById(R.id.dingwei_ok_ll);
		dingwei_explain_tv=(TextView)view.findViewById(R.id.dingwei_explain_tv);
		angew_dingwei_bt=(Button)view.findViewById(R.id.angew_dingwei_bt);
		
		
		
		location_tl=(RelativeLayout)view.findViewById(R.id.location_tl);
		home_eat_rl=(RelativeLayout)view.findViewById(R.id.home_eat_rl);
		home_fun_rl=(RelativeLayout)view.findViewById(R.id.home_fun_rl);
		home_sing_rl=(RelativeLayout)view.findViewById(R.id.home_sing_rl);
		city_name_tv=(TextView)view.findViewById(R.id.city_name_tv);
		search_iv=(ImageView)view.findViewById(R.id.search_iv);
		erweima_iv=(ImageView)view.findViewById(R.id.erweima_iv);
		hone_eat_recommend1=(LinearLayout)view.findViewById(R.id.hone_eat_recommend1);
		hone_eat_recommend2=(RelativeLayout)view.findViewById(R.id.hone_eat_recommend2);
		hone_eat_recommend3=(RelativeLayout)view.findViewById(R.id.hone_eat_recommend3);
		hone_eat_recommend4=(RelativeLayout)view.findViewById(R.id.hone_eat_recommend4);
		hone_eat_recommend5=(RelativeLayout)view.findViewById(R.id.hone_eat_recommend5);

		hone_fun_recommend1=(LinearLayout)view.findViewById(R.id.hone_fun_recommend1);
		hone_fun_recommend2=(RelativeLayout)view.findViewById(R.id.hone_fun_recommend2);
		hone_fun_recommend3=(RelativeLayout)view.findViewById(R.id.hone_fun_recommend3);
		hone_fun_recommend4=(RelativeLayout)view.findViewById(R.id.hone_fun_recommend4);
		hone_fun_recommend5=(RelativeLayout)view.findViewById(R.id.hone_fun_recommend5);

		hone_sing_recommend1=(LinearLayout)view.findViewById(R.id.hone_sing_recommend1);
		hone_sing_recommend2=(RelativeLayout)view.findViewById(R.id.hone_sing_recommend2);
		hone_sing_recommend3=(RelativeLayout)view.findViewById(R.id.hone_sing_recommend3);
		hone_sing_recommend4=(RelativeLayout)view.findViewById(R.id.hone_sing_recommend4);
		hone_sing_recommend5=(RelativeLayout)view.findViewById(R.id.hone_sing_recommend5);


		eat_shop_name_tv1=(TextView)view.findViewById(R.id.eat_shop_name_tv1);
		eat_shop_name_tv2=(TextView)view.findViewById(R.id.eat_shop_name_tv2);
		eat_shop_name_tv3=(TextView)view.findViewById(R.id.eat_shop_name_tv3);
		eat_shop_name_tv4=(TextView)view.findViewById(R.id.eat_shop_name_tv4);
		eat_shop_name_tv5=(TextView)view.findViewById(R.id.eat_shop_name_tv5);

		fun_shop_name_tv1=(TextView)view.findViewById(R.id.fun_shop_name_tv1);
		fun_shop_name_tv2=(TextView)view.findViewById(R.id.fun_shop_name_tv2);
		fun_shop_name_tv3=(TextView)view.findViewById(R.id.fun_shop_name_tv3);
		fun_shop_name_tv4=(TextView)view.findViewById(R.id.fun_shop_name_tv4);
		fun_shop_name_tv5=(TextView)view.findViewById(R.id.fun_shop_name_tv5);

		sing_shop_name_tv1=(TextView)view.findViewById(R.id.sing_shop_name_tv1);
		sing_shop_name_tv2=(TextView)view.findViewById(R.id.sing_shop_name_tv2);
		sing_shop_name_tv3=(TextView)view.findViewById(R.id.sing_shop_name_tv3);
		sing_shop_name_tv4=(TextView)view.findViewById(R.id.sing_shop_name_tv4);
		sing_shop_name_tv5=(TextView)view.findViewById(R.id.sing_shop_name_tv5);




		eat_shop_recommend_tv1=(TextView)view.findViewById(R.id.eat_shop_recommend_tv1);
		eat_shop_recommend_tv2=(TextView)view.findViewById(R.id.eat_shop_recommend_tv2);
		eat_shop_recommend_tv3=(TextView)view.findViewById(R.id.eat_shop_recommend_tv3);
		eat_shop_recommend_tv4=(TextView)view.findViewById(R.id.eat_shop_recommend_tv4);
		eat_shop_recommend_tv5=(TextView)view.findViewById(R.id.eat_shop_recommend_tv5);

		fun_shop_recommend_tv1=(TextView)view.findViewById(R.id.fun_shop_recommend_tv1);
		fun_shop_recommend_tv2=(TextView)view.findViewById(R.id.fun_shop_recommend_tv2);
		fun_shop_recommend_tv3=(TextView)view.findViewById(R.id.fun_shop_recommend_tv3);
		fun_shop_recommend_tv4=(TextView)view.findViewById(R.id.fun_shop_recommend_tv4);
		fun_shop_recommend_tv5=(TextView)view.findViewById(R.id.fun_shop_recommend_tv5);

		sing_shop_recommend_tv1=(TextView)view.findViewById(R.id.sing_shop_recommend_tv1);
		sing_shop_recommend_tv2=(TextView)view.findViewById(R.id.sing_shop_recommend_tv2);
		sing_shop_recommend_tv3=(TextView)view.findViewById(R.id.sing_shop_recommend_tv3);
		sing_shop_recommend_tv4=(TextView)view.findViewById(R.id.sing_shop_recommend_tv4);
		sing_shop_recommend_tv5=(TextView)view.findViewById(R.id.sing_shop_recommend_tv5);



		eat_shop_iv1=(ImageView)view.findViewById(R.id.eat_shop_iv1);
		eat_shop_iv2=(ImageView)view.findViewById(R.id.eat_shop_iv2);
		eat_shop_iv3=(ImageView)view.findViewById(R.id.eat_shop_iv3);
		eat_shop_iv4=(ImageView)view.findViewById(R.id.eat_shop_iv4);
		eat_shop_iv5=(ImageView)view.findViewById(R.id.eat_shop_iv5);

		fun_shop_iv1=(ImageView)view.findViewById(R.id.fun_shop_iv1);
		fun_shop_iv2=(ImageView)view.findViewById(R.id.fun_shop_iv2);
		fun_shop_iv3=(ImageView)view.findViewById(R.id.fun_shop_iv3);
		fun_shop_iv4=(ImageView)view.findViewById(R.id.fun_shop_iv4);
		fun_shop_iv5=(ImageView)view.findViewById(R.id.fun_shop_iv5);

		sing_shop_iv1=(ImageView)view.findViewById(R.id.sing_shop_iv1);
		sing_shop_iv2=(ImageView)view.findViewById(R.id.sing_shop_iv2);
		sing_shop_iv3=(ImageView)view.findViewById(R.id.sing_shop_iv3);
		sing_shop_iv4=(ImageView)view.findViewById(R.id.sing_shop_iv4);
		sing_shop_iv5=(ImageView)view.findViewById(R.id.sing_shop_iv5);

		eat_shop_address_tv1=(TextView)view.findViewById(R.id.eat_shop_address_tv1);
		fun_shop_address_tv1=(TextView)view.findViewById(R.id.fun_shop_address_tv1);
		sing_shop_address_tv1=(TextView)view.findViewById(R.id.sing_shop_address_tv1);
	}

	private void initListener() {
		location_tl.setOnClickListener(this);
		search_iv.setOnClickListener(this);
		erweima_iv.setOnClickListener(this);
		home_eat_rl.setOnClickListener(this);
		home_fun_rl.setOnClickListener(this);
		home_sing_rl.setOnClickListener(this);
		hone_eat_recommend1.setOnClickListener(this);
		hone_eat_recommend2.setOnClickListener(this);
		hone_eat_recommend3.setOnClickListener(this);
		hone_eat_recommend4.setOnClickListener(this);
		hone_eat_recommend5.setOnClickListener(this);

		hone_fun_recommend1.setOnClickListener(this);
		hone_fun_recommend2.setOnClickListener(this);
		hone_fun_recommend3.setOnClickListener(this);
		hone_fun_recommend4.setOnClickListener(this);
		hone_fun_recommend5.setOnClickListener(this);

		hone_sing_recommend1.setOnClickListener(this);
		hone_sing_recommend2.setOnClickListener(this);
		hone_sing_recommend3.setOnClickListener(this);
		hone_sing_recommend4.setOnClickListener(this);
		hone_sing_recommend5.setOnClickListener(this);
		angew_dingwei_bt.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.angew_dingwei_bt:
			dingwei();
			dingwei_explain_tv.setVisibility(View.VISIBLE);
			angew_dingwei_bt.setVisibility(View.GONE);
			break;
		case R.id.home_eat_rl:
			//			ToastUtil.makeLongText(getActivity(), "找吃");
			intent=new Intent(getActivity(), EatSeekActivity.class);
			startActivity(intent);
			
			break;
		case R.id.home_fun_rl:
			ToastUtil.makeLongText(getActivity(), "找玩");
			break;
		case R.id.home_sing_rl:
			ToastUtil.makeLongText(getActivity(), "找唱");
			break;
			//不要城市选择功能
//		case R.id.location_tl:
//			ToastUtil.makeLongText(getActivity(), "城市选择");
//			break;
		case R.id.search_iv:
			Intent i = new Intent(homeActibity, EatEaterySeekActivity.class);
			homeActibity.startActivity(i);
			break;
		case R.id.erweima_iv:
//			ToastUtil.makeLongText(getActivity(), "二维码");
			Intent erweimain = new Intent(homeActibity, CaptureActivity.class);
			homeActibity.startActivity(erweimain);
			break;
		case R.id.hone_eat_recommend1:
			ToastUtil.makeLongText(getActivity(), "商家推荐找吃1");
			break;
		case R.id.hone_eat_recommend2:
			ToastUtil.makeLongText(getActivity(), "商家推荐找吃2");
			break;
		case R.id.hone_eat_recommend3:
			ToastUtil.makeLongText(getActivity(), "商家推荐找吃3");
			break;
		case R.id.hone_eat_recommend4:
			ToastUtil.makeLongText(getActivity(), "商家推荐找吃4");
			break;
		case R.id.hone_eat_recommend5:
			ToastUtil.makeLongText(getActivity(), "商家推荐找吃5");
			break;
		case R.id.hone_fun_recommend1:
			ToastUtil.makeLongText(getActivity(), "商家推荐找玩1");
			break;
		case R.id.hone_fun_recommend2:
			ToastUtil.makeLongText(getActivity(), "商家推荐找玩2");
			break;
		case R.id.hone_fun_recommend3:
			ToastUtil.makeLongText(getActivity(), "商家推荐找玩3");
			break;
		case R.id.hone_fun_recommend4:
			ToastUtil.makeLongText(getActivity(), "商家推荐找玩4");
			break;
		case R.id.hone_fun_recommend5:
			ToastUtil.makeLongText(getActivity(), "商家推荐找玩5");
			break;
		case R.id.hone_sing_recommend1:
			ToastUtil.makeLongText(getActivity(), "商家推荐找唱1");
			break;
		case R.id.hone_sing_recommend2:
			ToastUtil.makeLongText(getActivity(), "商家推荐找唱2");
			break;
		case R.id.hone_sing_recommend3:
			ToastUtil.makeLongText(getActivity(), "商家推荐找唱3");
			break;
		case R.id.hone_sing_recommend4:
			ToastUtil.makeLongText(getActivity(), "商家推荐找唱4");
			break;
		case R.id.hone_sing_recommend5:
			ToastUtil.makeLongText(getActivity(), "商家推荐找唱5");
			break;
		}
	}


	//---------------------幻灯片-----------------------------
	//初始化
	void init(){
		myGallery = (DetailGallery)view.findViewById(R.id.myGallery);
		gallery_points = (RadioGroup) view.findViewById(R.id.galleryRaidoGroup);
		final ArrayList<Integer> list = new ArrayList<Integer>();
		//数据源
		/**电影项目数据源通过网络获取//获取幻灯片展示
		 */
		list.add(R.drawable.eat_lackmam);
		list.add(R.drawable.fun_leyouyou);
		list.add(R.drawable.fun_jinyelanjue);
		list.add(R.drawable.eat_champur);
		list.add(R.drawable.fun_huitang);
		GalleryIndexAdapter adapter = new GalleryIndexAdapter(list, homeActibity);
		myGallery.setAdapter(adapter);
		//点击监听
		myGallery.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				//					Toast.makeText(ImgSwitchActivity.this, "你选择了"+position+"号图片", Toast.LENGTH_SHORT).show();  
				switch (position) {
				case 0:
					Toast.makeText(homeActibity, "你点击了banner1==>>LACKLAM", 0).show();

					break;
				case 1:
					Toast.makeText(homeActibity, "你点击了banner2==>>乐忧游", 0).show();

					break;
				case 2:
					Toast.makeText(homeActibity, "你点击了banner3==>>金夜兰爵", 0).show();

					break;
				case 3:
					Toast.makeText(homeActibity, "你点击了banner4==>>CHUMPUR", 0).show();

					break;
				case 4:
					Toast.makeText(homeActibity, "你点击了banner4==>>灰汤温泉", 0).show();

					break;


				}
			}
		});
		//设置小按钮
		gallery_point = new RadioButton[list.size()];
		for (int i = 0; i < gallery_point.length; i++) {
			layout = (LinearLayout) inflater.inflate(R.layout.gallery_icon, null);
			gallery_point[i] = (RadioButton) layout.findViewById(R.id.gallery_radiobutton);
			gallery_point[i].setId(i);/* 设置指示图按钮ID */
			int wh = Tool.dp2px(homeActibity, 10);
			RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(wh, wh); // 设置指示图大小
			gallery_point[i].setLayoutParams(layoutParams);
			layoutParams.setMargins(4, 0, 4, 0);// 设置指示图margin值
			gallery_point[i].setClickable(false);/* 设置指示图按钮不能点击 */
			layout.removeView(gallery_point[i]);//一个子视图不能指定了多个父视图
			gallery_points.addView(gallery_point[i]);/* 把已经初始化的指示图动态添加到指示图的RadioGroup中 */
		}
	}
	//添加事件
	void addEvn(){
		myGallery.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				gallery_points.check(gallery_point[arg2%gallery_point.length].getId());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
	}
	/** 展示图控制器，实现展示图切换 */
	final Handler handler_gallery = new Handler() {
		public void handleMessage(Message msg) {
			/* 自定义屏幕按下的动作 */
			MotionEvent e1 = MotionEvent.obtain(SystemClock.uptimeMillis(),
					SystemClock.uptimeMillis(), MotionEvent.ACTION_UP,
					89.333336f, 265.33334f, 0);
			/* 自定义屏幕放开的动作 */
			MotionEvent e2 = MotionEvent.obtain(SystemClock.uptimeMillis(),
					SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN,
					300.0f, 238.00003f, 0);

			myGallery.onFling(e2, e1, -800, 0);
			/* 给gallery添加按下和放开的动作，实现自动滑动 */
			super.handleMessage(msg);
		}
	};
	protected void getdata() {
		autogallery();
		super.onResume();
	};
	private void autogallery() {
		/* 设置定时器，每5秒自动切换展示图 */
		Timer time = new Timer();
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				Message m = new Message();
				handler_gallery.sendMessage(m);
			}
		};
		time.schedule(task, 8000, 5000);
	}
	public class GalleryIndexAdapter extends BaseAdapter {
		List<Integer> imagList;
		Context context;
		public GalleryIndexAdapter(List<Integer> list,Context cx){
			imagList = list;
			context = cx;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Integer.MAX_VALUE;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return imagList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			ImageView imageView = new ImageView(context);
			imageView.setImageResource(imagList.get(position%imagList.size()));  
			imageView.setScaleType(ScaleType.FIT_XY);
			imageView.setLayoutParams(new Gallery.LayoutParams(Gallery.LayoutParams.FILL_PARENT
					, Gallery.LayoutParams.WRAP_CONTENT));
			// 给ImageView设置资源  
			return imageView;
		}	
	}




	//-------------------地图定位---------------------------
	/**
	 * 初始化定位
	 */
	private void dingwei() {      
		mLocationManagerProxy = LocationManagerProxy.getInstance(homeActibity);
		//此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
		//注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
		//在定位结束后，在合适的生命周期调用destroy()方法     
		//其中如果间隔时间为-1，则定位只定一次
		mLocationManagerProxy.requestLocationData(
				LocationProviderProxy.AMapNetwork, -1, 15, this);
		mLocationManagerProxy.setGpsEnable(false);
		showWaitDialog();
		dingwei_explain_tv.setVisibility(View.VISIBLE);
	}
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}
	/**
	 * 定位成功后回调函数
	 */
	@Override
	public void onLocationChanged(AMapLocation amapLocation) {
		if(amapLocation != null && amapLocation.getAMapException().getErrorCode() == 0){
			//获取位置信息
			MyApplication.addl=new AddressLocation();
			//纬度
			double geoLat = amapLocation.getLatitude();
			MyApplication.addl.setGeoLat(geoLat);
			System.out.println("定位纬度==》》"+MyApplication.addl.getGeoLat());
			
			//经度
			double geoLng = amapLocation.getLongitude();   
			MyApplication.addl.setGeoLng(geoLng);
			System.out.println("定位经度==》》"+MyApplication.addl.getGeoLat());
			//获取定位速度
			float speed = amapLocation.getSpeed();   
			MyApplication.addl.setSpeed(speed);
			System.out.println("获取定位速度==》》"+MyApplication.addl.getSpeed());
			//方向
			float bearing = amapLocation.getBearing();
			MyApplication.addl.setBearing(bearing);
			System.out.println("定位方向==》》"+bearing);
			//省名称
			String province = amapLocation.getProvince();  
			MyApplication.addl.setProvince(province);
			System.out.println("省份名称==》》"+province);
			//城市名称
			String city = amapLocation.getCity();   
			MyApplication.addl.setCity(city);
			System.out.println("城市名称==》》"+city);
			//城市编码
			String code = amapLocation.getCityCode();
			MyApplication.addl.setCode(code);
			System.out.println("城市编码==》》"+code);
			//区（县）名称
			district = amapLocation.getDistrict(); 
			MyApplication.addl.setDistrict(district);
			city_name_tv.setText(district);
			System.out.println("区（县）名称==》》"+district);
			//区域编码
			String adcode = amapLocation.getAdCode(); 
			MyApplication.addl.setAdcode(adcode);
			System.out.println("区域编码==》》"+adcode);
			//街道和门牌信息
			String street = amapLocation.getStreet(); 
			MyApplication.addl.setStreet(street);
			System.out.println("街道和门牌信息==》》"+street);
			//详细地址
			String address = amapLocation.getAddress(); 
			MyApplication.addl.setAddress(address);
			System.out.println("详细地址==》》"+MyApplication.addl.getAddress());
			//描述信息
			Bundle extras = amapLocation.getExtras();   
			MyApplication.addl.setExtras(extras);
			System.out.println("描述信息==》》"+extras.toString());
			dingwei_ok_ll.setVisibility(View.VISIBLE);
			dingwei_no_ll.setVisibility(View.GONE);
			angew_dingwei_bt.setVisibility(View.GONE);
			dingwei_explain_tv.setVisibility(View.GONE);
			ditumark=1;
		}else {
			dingwei_no_ll.setVisibility(View.VISIBLE);
			dingwei_ok_ll.setVisibility(View.GONE);
			angew_dingwei_bt.setVisibility(View.VISIBLE);
			dingwei_explain_tv.setVisibility(View.GONE);
			ditumark=2;
			ToastUtil.makeText(homeActibity, "定位失败，请检查网络与GPS", 0);
		}
		
		dismissDialog();
	}
	/**
	 * 停止定位
	 */
	public void deactivate() {
		if (mLocationManagerProxy != null) {
			mLocationManagerProxy.removeUpdates(this);
			mLocationManagerProxy.destroy();
		}
		mLocationManagerProxy = null;
	}
}
