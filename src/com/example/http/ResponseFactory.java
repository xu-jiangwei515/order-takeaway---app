package com.example.http;

/**
 * 对各个接口返回的json数据进行处理的工厂类.
 * 
 * @author zhang.zk
 * 
 */
public class ResponseFactory {
	//经度
	public static final String LONGITUDE = "longitude";
	//纬度
	public static final String LATITUDE = "latitude";
	
	/**性别*/
	public static final String SEX = "sex";
	/**
	 * 相册名.
	 */
	public static final String ALBUM_NAME = "album_name";
	/**
	 * 相册数据.
	 */
	public static final String ALBUM_LIST = "album_list";
	/**
	 * 地区id
	 */
	public static final String CID = "cid";
	/**
	 * 地区名
	 */
	public static final String CNAME = "cname";
	/**
	 * 联系人昵称
	 */
	public static final String UNAME = "uname";
	/**
	 * 创建人昵称
	 */
	public static final String SNAME = "sname";
	/** 出价 价格 */
	public static final String BIDDER = "bidder";


	/**
	 * 判断返回的结果是否是成功的.初步解析result和message,如果成功(result!=0)返回true,调用该方法的代码应该继续解析data
	 * .否则,返回false,data不用再解析.
	 * 
	 * @param bundle
	 * @param jsonObject
	 * @return
	 * @throws JSONException
	 */
//	private static boolean isResultSuccess(Bundle bundle, JSONObject jsonObject)
//			throws JSONException {
//		boolean isSuc = false;
//		if (jsonObject.has(STATE)) {
//			int state = jsonObject.optInt(STATE);
//			bundle.putInt(STATE, state);
//			if (state == 1) {
//				isSuc = true;
//			}
//			if (jsonObject.has(MESSAGE)) {
//				bundle.putString(MESSAGE, jsonObject.optString(MESSAGE));
//			}
//		}
//		return isSuc;
//	}

	/**
	 * 1.1获取验证码
	 * 
	 * @param wsResponse
	 * @return
	 * @throws JSONException
	 */
//	public static Bundle parseGetCodeResult(String wsResponse)
//			throws JSONException {
//		JSONObject jsonObject = new JSONObject(wsResponse);
//		Bundle bundle = new Bundle();
//		if (!isResultSuccess(bundle, jsonObject)) {// 如果result==0,不再解析data,直接返回bundle;
//			return bundle;
//		}
//		bundle.putString(CODE, jsonObject.optString(CODE));
//		return bundle;
//	}

	/**
	 * 1.2地区
	 * 
	 * @param response
	 * @return
	 * @throws JSONException
	 */
//	public static Bundle parseCityResult(String response) throws JSONException {
//		JSONObject jsonObject = new JSONObject(response);
//		Bundle bundle = new Bundle();
//		if (!isResultSuccess(bundle, jsonObject)) {// 如果result==0,不再解析data,直接返回bundle;
//			return bundle;
//		}
//		ArrayList<Map<String, String>> provinces = new ArrayList<Map<String, String>>();
//		JSONObject listObject = jsonObject.optJSONObject(LIST);
//		if (listObject != null) {
//			Iterator<String> keys = listObject.keys();
//			while (keys.hasNext()) {
//				String next = keys.next();
//				JSONArray array = listObject.optJSONArray(next);
//				for (int i = 0; i < array.length(); i++) {
//					JSONObject obj = array.getJSONObject(i);
//					Iterator<String> key = obj.keys();
//					if (key.hasNext()) {
//						String province = key.next();
//						System.out.println("省："+province);
//						JSONArray cityArray = obj.optJSONArray(province);
//						for (int j = 0; j < cityArray.length(); j++) {
//							JSONObject cityObj = cityArray.getJSONObject(j);
//							System.out.println("省的城市   "+cityObj.toString());
//							String city = cityObj.optString(NAME);
//							String id = cityObj.optString(ID);
//							HashMap<String, String> map = new HashMap<String, String>();
//							map.put(CityActivity.PROVINCE_NAME, province);
//							map.put(CityActivity.CITY_NAME, city);
//							map.put(CityActivity.CITY_ID, id);
//							provinces.add(map);
//						}
//					}
//				}
//			}
//			/**
//			 * 对省市进行排序
//			 */
//			Collections.sort(provinces, ComparatorUtil.getCityComparator());
//			bundle.putSerializable(PROVINCES, provinces);
//		}
//
//		return bundle;
//	}


}
