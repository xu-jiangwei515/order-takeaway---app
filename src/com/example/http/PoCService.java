package com.example.http;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.llkj.cm.restfull.exception.RestClientException;
import com.llkj.cm.restfull.service.WorkerService;

/**
 * This class is called by the {@link PoCRequestManager} through the
 * {@link Intent} system. Get the parameters stored in the {@link Intent} and
 * call the right Worker. f
 * 
 * @author Foxykeep
 */
public class PoCService extends WorkerService {

	private static final String LOG_TAG = PoCService.class.getSimpleName();

	// Max number of parallel threads used
	private static final int MAX_THREADS = 5;

	public static final int ADD_CONTROL_TYPE_COMMODITY = 1;
	public static final int ADD_CONTROL_TYPE_PT = 2;
	public static final int ADD_CONTROL_TYPE_PHOTO = 3;
	public static final int ADD_CONTROL_TYPE_TEXT = 4;
	public static final int ADD_CONTROL_TYPE_VOICE = 5;
	public static final int ADD_CONTROL_TYPE_CURRENT_PAGE = 6;
	public static final int ADD_CONTROL_TYPE_ROUTE = 7;
	public static final int ADD_CONTROL_TYPE_ADDRESS_WALL = 8;
	public static final int ADD_CONTROL_TYPE_GROUP = 9;
	public static final int ADD_CONTROL_TYPE_WEBPAGE = 10;

	// Worker types
	/**
	 * 1.1获取验证码
	 */
	public static final int TYPE_INDEX_GETCODE = 0x11;
	/**
	 * 1.2地区
	 */
	public static final int TYPE_INDEX_CITY = 0x12;
	/**
	 * 1.3登陆
	 */
	public static final int TYPE_INDEX_DO_LOGIN = 0x13;
	/**
	 * 1.4注册
	 */
	public static final int TYPE_INDEX_REGISTER = 0x14;
	/**
	 * 1.5增加好友设置
	 */
	public static final int TYPE_INDEX_FRIEND_SET = 0x15;
	/**
	 * 1.6找回密码
	 */
	public static final int TYPE_INDEX_FORGET_PWD = 0x16;
	/**
	 * 2.1我的信息
	 */
	public static final int TYPE_PERSON_MY_INFO = 0x21;
	/**
	 * 2.2我的收藏
	 */
	public static final int TYPE_PERSON_MY_COLLECT = 0x22;
	/**
	 * 2.3我的兴趣
	 */
	public static final int TYPE_PERSON_MY_INTEREST = 0x23;
	/**
	 * 2.4搜索画家
	 */
	public static final int TYPE_PERSON_SEARCH_ARTIST = 0x24;
	/**
	 * 2.5相册（我的，他人）
	 */
	public static final int TYPE_PERSON_MY_PHOTO = 0x25;
	/**
	 * 2.6我的画廊
	 */
	public static final int TYPE_PERSON_MY_GALLERY = 0x26;
	/**
	 * 2.7意见反馈
	 */
	public static final int TYPE_PERSON_ADVISE = 0x27;
	/**
	 * 2.8上传图片
	 */
	public static final int TYPE_PERSON_UPLOAD_PIC = 0x28;
	/**
	 * 2.9修改我的信息
	 */
	public static final int TYPE_PERSON_INFO_EDIT = 0x29;
	/**
	 * 2.10取消或兴趣
	 */
	public static final int TYPE_PERSON_INTEREST = 0x210;
	/**
	 * 2.11删除相册
	 */
	public static final int TYPE_PERSON_DELETE_PHOTO = 0x211;
	/**
	 * 2.12售出
	 */
	public static final int TYPE_PERSON_SALE = 0x212;
	/**
	 * 2.13画廊详情
	 */
	public static final int TYPE_PERSON_GALLERY_DESC = 0x213;
	/**
	 * 2.14修改画廊
	 */
	public static final int TYPE_PERSON_EDIT_GALLERY = 0x214;
	/**
	 * 2.15新增画廊
	 */
	public static final int TYPE_PERSON_ADD_GALLERY = 0x215;
	/**
	 * 2.16隐私设置
	 */
	public static final int TYPE_PERSON_PRIVACY_SET = 0x216;
	/**
	 * 2.17不让他看画友圈 (朋友圈权限)
	 */
	public static final int TYPE_PERSON_BLACKLIST = 0x217;
	/**
	 * 2.18不看他画友圈(朋友圈权限)
	 */
	public static final int TYPE_PERSON_NO_LOOK = 0x218;
	/**
	 * 2.19修改密码
	 */
	public static final int TYPE_PERSON_EDIT_PWD = 0x219;
	/**
	 * 2.20版本更新
	 */
	public static final int TYPE_PERSON_VERSION = 0x220;
	/**
	 * 2.21聊天备份与下载
	 */
	public static final int TYPE_PERSON_BACKUPS = 0x221;
	/**
	 * 2.22设置不让他看
	 */
	public static final int TYPE_PERSON_BLACK_MORE = 0x222;
	/**
	 * 2.23设置不看他
	 */
	public static final int TYPE_PERSON_LOOK_MORE = 0x223;
	/**
	 * 2.24查看不让他看
	 */
	public static final int TYPE_PERSON_GET_BLACK = 0x224;
	/**
	 * 2.25查看不看他
	 */
	public static final int TYPE_PERSON_GET_LOOK = 0x225;
	/**
	 * 2.26查看隐私设置
	 */
	public static final int TYPE_PERSON_GET_PRIVACY = 0x226;
	/**
	 * 3.1我的群列表
	 */
	public static final int TYPE_CHAT_GROUPLIST = 0x31;
	/**
	 * 3.2群用户
	 */
	public static final int TYPE_CHAT_GROUP_USERS = 0x32;
	/**
	 * 3.3创建群
	 */
	public static final int TYPE_CHAT_ADD_GROUP = 0x33;
	/**
	 * 3.4加入群
	 */
	public static final int TYPE_CHAT_JOIN_GROUP = 0x34;
	/**
	 * 3.5退出群
	 */
	public static final int TYPE_CHAT_BYE_GROUP = 0x35;

	/**
	 * 3.6注册成功后调用
	 */
	public static final int TYPE_CHAT_SET_DEVICE_TOKEN = 0x36;
	/**
	 * 3.7系统消息
	 */
	public static final int TYPE_CHAT_SYSTEM = 0x37;
	/**
	 * 3.8修改群名称
	 */
	public static final int TYPE_CHAT_GROUP_NAME = 0x38;

	/**
	 * 4.1是否有新信息（兴趣，画友）
	 */
	public static final int TYPE_PICTURE_NEW_INFO = 0x41;
	/**
	 * 4.2广场
	 */
	public static final int TYPE_PICTURE_PIAZZA = 0x42;
	/**
	 * 4.3兴趣圈
	 */
	public static final int TYPE_PICTURE_INTEREST_PEN = 0x43;
	/**
	 * 4.4画友圈
	 */
	public static final int TYPE_PICTURE_PICTURE_PEN = 0x44;
	/**
	 * 4.5拍品详情
	 */
	public static final int TYPE_PICTURE_AUCTION_DETAIL = 0x45;
	/**
	 * 4.6拍品描述
	 */
	public static final int TYPE_PICTURE_AUCTION_DESCRIBE = 0x46;
	/**
	 * 4.7出售信息
	 */
	public static final int TYPE_PICTURE_SELL_INFO = 0x47;
	/**
	 * 4.8点赞或取消
	 */
	public static final int TYPE_PICTURE_PRAISE = 0x48;
	/**
	 * 4.9文本详情
	 */
	public static final int TYPE_PICTURE_TEXT_DETAIL = 0x49;
	/**
	 * 4.10投诉
	 */
	public static final int TYPE_PICTURE_COMPLAIN = 0x410;
	/**
	 * 4.11评论
	 */
	public static final int TYPE_PICTURE_COMMENT = 0x411;
	/**
	 * 4.12收藏
	 */
	public static final int TYPE_PICTURE_COLLECT = 0x412;
	/**
	 * 4.13关注拍品
	 */
	public static final int TYPE_PICTURE_ATTENTION = 0x413;
	/**
	 * 4.14出价
	 */
	public static final int TYPE_PICTURE_BID = 0x414;
	/**
	 * 4.15回复出价
	 */
	public static final int TYPE_PICTURE_BID_REPLY = 0x415;
	/**
	 * 4.16发布出售信息
	 */
	public static final int TYPE_PICTURE_ISSUE_SELL = 0x416;
	/**
	 * 4.17发布拍卖信息
	 */
	public static final int TYPE_PICTURE_ISSUE_AUCTION = 0x417;
	/**
	 * 4.18发布其他信息
	 */
	public static final int TYPE_PICTURE_ISSUE_RESTS = 0x418;
	/**
	 * 4.19出价详情
	 */
	public static final int TYPE_PICTURE_COMMENT_DETAIL = 0x419;
	/**
	 * 5.1我的画友
	 */
	public static final int TYPE_FRIEND_MY_FRIEND = 0x51;
	/**
	 * 5.2新的好友
	 */
	public static final int TYPE_FRIEND_NEW_FRIEND = 0x52;
	/**
	 * 5.3读取通讯录
	 */
	public static final int TYPE_FRIEND_CONTACTS = 0x53;
	/**
	 * 5.4画友详细资料
	 */
	public static final int TYPE_FRIEND_FRIEND_INFO = 0x54;
	/**
	 * 5.5设置备注名
	 */
	public static final int TYPE_FRIEND_REMARK = 0x55;
	/**
	 * 5.6加好友
	 */
	public static final int TYPE_FRIEND_ADD_FRIEND = 0x56;
	/**
	 * 5.7通过验证
	 */
	public static final int TYPE_FRIEND_VERIFY = 0x57;
	/**
	 * 5.8加入黑名单
	 */
	public static final int TYPE_FRIEND_ADD_BLACKLIST = 0x58;
	/**
	 * 5.9删除画友
	 */
	public static final int TYPE_FRIEND_FRIEND_DEL = 0x59;
	/**
	 * 5.10查看朋友圈权限
	 */
	public static final int TYPE_FRIEND_GET_PEN = 0x510;
	/**
	 * 5.11搜索增加好友
	 */
	public static final int TYPE_FRIEND_SEARCH = 0x511;

	/**
	 * 6.1印章查询
	 */
	public static final int TYPE_FRIEND_SEARCH_SIGNET = 0x61;
	/**
	 * 6.2搜画友
	 */
	public static final int TYPE_SET_SEARCH_FRIEND = 0x62;
	/**
	 * 6.3搜画廊
	 */
	public static final int TYPE_SET_SEARCH_GALLERY = 0x63;
	/**
	 * 6.4附近画廊
	 */
	public static final int TYPE_SET_NEARBY_GALLERY = 0x64;
	/**
	 * 6.5附近画友
	 */
	public static final int TYPE_SET_NEARBY_FRIEND = 0x65;
	/**
	 * 6.6画廊详情
	 */
	public static final int TYPE_SET_GALLERY_INFO = 0x66;
	/**
	 * 6.7各地画廊
	 */
	public static final int TYPE_SET_PLACE_GALLERY = 0x67;
	/**
	 * 6.8印章详情
	 */
	public static final int TYPE_SET_SIGNET_INFO = 0x68;
	/**
	 * 6.9扫一扫
	 */
	public static final int TYPE_SET_SCAN_CODE = 0x69;
	/**
	 * 6.10好友资料
	 */
	public static final int TYPE_SET_FRIEND_DESC = 0x610;
	/**
	 * 6.11认领画廊
	 */
	public static final int TYPE_SET_CLAIM_GALLERY = 0x611;
	/**
	 * 6.12某城市画廊
	 */
	public static final int TYPE_SET_GALLERY_CITY = 0x612;
	/**
	 * 6.13联系店主
	 */
	public static final int TYPE_SET_CONTACT_US = 0x613;
	/**
	 * 6.14搜卖家
	 */
	public static final int TYPE_SET_SELLER = 0x614;
	/**
	 * 6.15搜买家
	 */
	public static final int TYPE_SET_SEARCH_BUYER = 0x615;
	/**
	 * 7.1相册
	 */
	public static final int TYPE_ANDROID_PHOTO = 0x71;
	/**
	 * 7.2地区
	 */
	public static final int TYPE_ANDROID_AREA = 0x72;
	/**
	 * 7.3我的画友
	 */
	public static final int TYPE_ANDROID_FRIEND = 0x73;
	/**
	 * 7.4各地画廊
	 */
	public static final int TYPE_ANDROID_GALLERY = 0x74;
	/** 7.8我的参拍 */
	public static final int TYPE_ANDROID_MYAUCTION = 0X75;
	/**
	 * 4.21 取消收藏
	 */
	public static final int TYPE_PICTURE_COLLECT_DEL = 0x76;
	/**
	 * 2.27删除评论
	 */
	public static final int TYPE_PERSON_DELETE_COMMENT = 0x77;

	/**
	 * 3.9获取群头像
	 */
	public static final int TYPE_CHAT_GROUP_LOGO = 0x78;

	/**
	 * 7.5是否有消息
	 */
	public static final int TYPE_ANDROID_INFORMATION = 0x79;
	/**
	 * 7.6消息列表
	 */
	public static final int TYPE_ANDROID_INFORMATION_LIST = 0x80;
	/**
	 * 7.7清空消息
	 */
	public static final int TYPE_ANDROID_INFORMATION_DEL = 0x81;
	/**
	 * 4.20取消关注拍品
	 */
	public static final int ATTENTION_DEL = 0x420;
	/**
	 * 4.21是否存在画廊
	 */
	public static final int IS_OWN = 0x421;
	/**
	 * 不封装城市
	 */
	public static final int ON_CTIY=0x422;
	public PoCService() {
		super(MAX_THREADS);
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
		// This line will generate the Android User Agent which will be used in
		// your webservice calls if you don't specify a special one
		// NetworkConnection.generateDefaultUserAgent(this);

		int workerType = intent.getIntExtra(INTENT_EXTRA_WORKER_TYPE, -1);

		try {
			switch (workerType) {
			// TODO 可以考虑在返回方法里增加文件缓存.参见新浪乐居FixCacheManger
			case TYPE_INDEX_GETCODE:
//				sendSuccess(intent, HTTPWorker.getCode(intent));
				break;
			case TYPE_INDEX_CITY:
//				sendSuccess(intent, HTTPWorker.city(intent));
				break;
			}
		} catch (final IllegalStateException e) {
			Log.e(LOG_TAG, "IllegalStateException", e);
			sendConnexionFailure(intent, null);
		} 
		// This block (which should be the last one in your implementation)
		// will catch all the RuntimeException and send you back an error
		// that you can manage. If you remove this catch, the
		// RuntimeException will still crash the PoCService but you will not be
		// informed (as it is in 'background') so you should never remove this
		// catch
		catch (final RuntimeException e) {
			Log.e(LOG_TAG, "RuntimeException", e);
			sendDataFailure(intent, null);
		}
	}
}
