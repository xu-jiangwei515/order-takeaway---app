package com.example.eat.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.byl.testdate.widget.NumericWheelAdapter;
import com.byl.testdate.widget.OnWheelScrollListener;
import com.byl.testdate.widget.WheelView;
import com.example.dao.MyApplication;
import com.example.eat.adapter.DateAdapter;
import com.example.eat.adapter.EatSortTypeAdapter;
import com.example.eat.adapter.EatSortTypeToAdapter;
import com.example.eat.adapter.TimeChooseAdapter;
import com.example.entity.AddressLocation;
import com.example.entity.Site;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.AddSiteActivity;
import com.example.seeksubscribe.Activity.BaseActivity;
import com.example.utils.SpecialCalendar;
import com.example.utils.ToastUtil;
//预订饭店座位
public class EatShopCustomTableActivity extends BaseActivity implements OnClickListener,OnGestureListener{

	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private String shopName;
	//	private TextView grace_room_quantity_tv;
	//	private TextView hall_room_quantity_tv;
	private TextView choose_time_tv;
	private TextView show_choose_time_tv;
	private RelativeLayout people_quantity_ll;
	private EditText show_people_quantity_tv;
	//	private EditText people_name_et;
	//	private RadioButton man;
	//	private RadioButton woman;
	//	private CheckBox grace_room_cb;
	//	private CheckBox hall_room_cb;
	private Button submit_shop_ok_bt;
	private ViewFlipper flipper1 = null;
	private static String TAG = "ZzL";
	private GridView gridView = null;
	private GestureDetector gestureDetector = null;
	private int year_c = 0;
	private int month_c = 0;
	private int day_c = 0;
	private int week_c = 0;
	private int week_num = 0;
	private String currentDate = "";
	private static int jumpWeek = 0;
	private static int jumpMonth = 0;
	private static int jumpYear = 0;
	private DateAdapter dateAdapter;
	private int daysOfMonth = 0; // 某月的天数
	private int dayOfWeek = 0; // 具体某一天是星期几
	private int weeksOfMonth = 0;
	private SpecialCalendar sc = null;
	private boolean isLeapyear = false; // 是否为闰年
	private int selectPostion = 0;
	private String dayNumbers[] = new String[7];
	private TextView tvDate;
	private int currentYear;
	private int currentMonth;
	private int currentWeek;
	private int currentDay;
	private int currentNum;
	private boolean isStart;// 是否是交接的月初
	//	private RadioGroup xingbie_rg;
	//女人为0，男人为1
	private int gender=0;
	//0为包间，1为大厅
	//	private int roomType;
	private PopupWindow pw;
	//订餐时间
	private String time;
	private Button eatshopCTA_ll;
	//	private EditText people_number_et;
	private TextView sun_money_tv;
	private String sun_money;
	private TextView last_weel_tv;
	private TextView next_weel_tv;
	//时间选择器
	PopupWindow menuWindow;
	private LayoutInflater inflater;
	private WheelView year;
	private WheelView month;
	private WheelView day;
	private WheelView hour;
	private WheelView mins;
	private LinearLayout address_ll;
	private TextView number_tel_tv;
	private TextView people_name_tv;
	private TextView site_tv;



	public EatShopCustomTableActivity() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
		currentDate = sdf.format(date);
		year_c = Integer.parseInt(currentDate.split("-")[0]);
		month_c = Integer.parseInt(currentDate.split("-")[1]);
		day_c = Integer.parseInt(currentDate.split("-")[2]);
		currentYear = year_c;
		currentMonth = month_c;
		currentDay = day_c;
		sc = new SpecialCalendar();
		getCalendar(year_c, month_c);
		week_num = getWeeksOfMonth();
		currentNum = week_num;
		if (dayOfWeek == 7) {
			week_c = day_c / 7 + 1;
		} else {
			if (day_c <= (7 - dayOfWeek)) {
				week_c = 1;
			} else {
				if ((day_c - (7 - dayOfWeek)) % 7 == 0) {
					week_c = (day_c - (7 - dayOfWeek)) / 7 + 1;
				} else {
					week_c = (day_c - (7 - dayOfWeek)) / 7 + 2;
				}
			}
		}
		currentWeek = week_c;
		getCurrent();

	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eat_shop_custom_table);
		inflater=LayoutInflater.from(EatShopCustomTableActivity.this);
		findviewByid();
		initData();
		init();
		initListenter();
		date();
		jiancezuowei();
	}
	private void jiancezuowei() {
		//发送请求获取数据包含剩余桌位(注销无座位提示排队功能)
		//		grace_room_quantity_tv.setText("0");
		//		hall_room_quantity_tv.setText("20");
		//		if(Integer.parseInt(grace_room_quantity_tv.getText().toString())<=0){
		//			Builder builder = new AlertDialog.Builder(this);
		//			builder.setTitle("提示");
		//			builder.setMessage("当前无包间，但可预定，需要排队");
		//			builder.setPositiveButton("关闭", new DialogInterface.OnClickListener() {
		//				
		//				@Override
		//				public void onClick(DialogInterface dialog, int which) {
		//					dialog.dismiss();
		//				}
		//			});
		//			builder.create().show();
		//		}else if(Integer.parseInt(hall_room_quantity_tv.getText().toString())<=0){
		//			Builder builder = new AlertDialog.Builder(this);
		//			builder.setTitle("提示");
		//			builder.setMessage("当前无卡座，但可预定，需要排队");
		//			builder.setPositiveButton("关闭", new DialogInterface.OnClickListener() {
		//				
		//				@Override
		//				public void onClick(DialogInterface dialog, int which) {
		//					dialog.dismiss();
		//				}
		//			});
		//			builder.create().show();
		//		}else if(Integer.parseInt(hall_room_quantity_tv.getText().toString())<=0&&Integer.parseInt(grace_room_quantity_tv.getText().toString())<=0){
		//			Builder builder = new AlertDialog.Builder(this);
		//			builder.setTitle("提示");
		//			builder.setMessage("当前餐厅无座位，但可预定，需要排队");
		//			builder.setPositiveButton("关闭", new DialogInterface.OnClickListener() {
		//				
		//				@Override
		//				public void onClick(DialogInterface dialog, int which) {
		//					dialog.dismiss();
		//				}
		//			});
		//			builder.create().show();
		//		}


	}
	private void date() {
		tvDate = (TextView)findViewById(R.id.tv_date);
		tvDate.setText(year_c + "年" + month_c + "月" + day_c + "日");
		gestureDetector = new GestureDetector(this);
		flipper1 = (ViewFlipper) findViewById(R.id.flipper1);
		dateAdapter = new DateAdapter(this, getResources(), currentYear,
				currentMonth, currentWeek, currentNum, selectPostion,
				currentWeek == 1 ? true : false);
		addGridView();
		dayNumbers = dateAdapter.getDayNumbers();
		gridView.setAdapter(dateAdapter);
		selectPostion = dateAdapter.getTodayPosition();
		gridView.setSelection(selectPostion);
		flipper1.addView(gridView, 0);
	}
	private void findviewByid() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//雅间数量
		//		grace_room_quantity_tv=(TextView)findViewById(R.id.grace_room_quantity_tv);
		//卡座数量
		//		hall_room_quantity_tv=(TextView)findViewById(R.id.hall_room_quantity_tv);
		//选择时间
		choose_time_tv=(TextView)findViewById(R.id.choose_time_tv);
		//显示选择好的时间
		show_choose_time_tv=(TextView)findViewById(R.id.show_choose_time_tv);
		//选择人数布局
		people_quantity_ll=(RelativeLayout)findViewById(R.id.people_quantity_ll);
		//显示选择的人数
		show_people_quantity_tv=(EditText)findViewById(R.id.show_people_quantity_tv);
		//订餐人姓名输入框
		//		people_name_et=(EditText)findViewById(R.id.people_name_et);
		//订餐人电话号码
		//		people_number_et=(EditText)findViewById(R.id.people_number_et);
		//选择男女radioGroup
		//		xingbie_rg=(RadioGroup)findViewById(R.id.xingbie_rg);
		//男士
		//		man=(RadioButton)findViewById(R.id.man);
		//女士
		//		woman=(RadioButton)findViewById(R.id.woman);
		//选择类型：包间
		//		grace_room_cb=(CheckBox)findViewById(R.id.grace_room_cb);
		//选择类型：卡座
		//		hall_room_cb=(CheckBox)findViewById(R.id.hall_room_cb);
		//提交预订
		submit_shop_ok_bt=(Button)findViewById(R.id.submit_shop_ok_bt);
		//共计多少元
		sun_money_tv=(TextView)findViewById(R.id.sun_money_tv);
		//上一个星期
		last_weel_tv=(TextView)findViewById(R.id.last_weel_tv);
		//下一个星期
		next_weel_tv=(TextView)findViewById(R.id.next_weel_tv);
		//点击修改收货地址
		address_ll=(LinearLayout)findViewById(R.id.address_ll);
		//收货人电话
		number_tel_tv=(TextView)findViewById(R.id.number_tel_tv);
		//收货人姓名
		people_name_tv=(TextView)findViewById(R.id.people_name_tv);
		//收货人地址
		site_tv=(TextView)findViewById(R.id.site_tv);
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);
	}
	private void initData() {
		sun_money_tv.setText(MyApplication.getInstance().sun_money);
	}
	private void init() {
		title_location_tv.setText("咖喱大叔");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);
	}
	private void initListenter() {
		submit_shop_ok_bt.setOnClickListener(this);
		title_return_rl.setOnClickListener(this);
		choose_time_tv.setOnClickListener(this);
		last_weel_tv.setOnClickListener(this);
		next_weel_tv.setOnClickListener(this);
		address_ll.setOnClickListener(this);
		//		xingbie_rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//			@Override
		//			public void onCheckedChanged(RadioGroup arg0, int checkedId) {
		//				//女人为0，男人为1
		//				gender= checkedId==R.id.woman?0:1;
		//				System.out.println(gender);
		//			}
		//		});
		/**
		 * 注释掉选包间还是选座功能
		 */
		//		grace_room_cb.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
		//
		//
		//			@Override
		//			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		//				if(arg1){
		//					hall_room_cb.setChecked(false);
		//					//预订类型type 0为包间，1为大厅
		//					roomType=0;
		//				}else {
		//					hall_room_cb.setChecked(true);
		//					roomType=1;
		//				}
		//				System.out.println(roomType);
		//			}
		//		});
		//		hall_room_cb.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
		//
		//
		//			@Override
		//			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		//				if(arg1){
		//					grace_room_cb.setChecked(false);
		//					//预订类型type 0为包间，1为大厅
		//					roomType=1;
		//				}else {
		//					grace_room_cb.setChecked(true);
		//					roomType=0;
		//				}
		//				System.out.println(roomType);
		//			}
		//		});
	}

	@Override
	public void onClick(View v) {
		int gvFlag = 0;
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.choose_time_tv:
			showPopwindow(getTimePick());//弹出时间选择器
			//			showChoiseDialog();
			break;
		case R.id.address_ll:
			Intent i = new Intent(EatShopCustomTableActivity.this, AddSiteActivity.class);
			i.putExtra("mark", 101);
			startActivityForResult(i, 101);
			break;
			//		case R.id.last_weel_tv://上一个星期
			//			addGridView();
			//			currentWeek--;
			//			getCurrent();
			//			dateAdapter = new DateAdapter(this, getResources(), currentYear,
			//					currentMonth, currentWeek, currentNum, selectPostion,
			//					currentWeek == 1 ? true : false);
			//			dayNumbers = dateAdapter.getDayNumbers();
			//			gridView.setAdapter(dateAdapter);
			//			tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "年"
			//					+ dateAdapter.getCurrentMonth(selectPostion) + "月"
			//					+ dayNumbers[selectPostion] + "日");
			//			gvFlag++;
			//			flipper1.addView(gridView, gvFlag);
			//			dateAdapter.setSeclection(selectPostion);
			//			this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
			//					R.anim.push_right_in));
			//			this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
			//					R.anim.push_right_out));
			//			this.flipper1.showPrevious();
			//			flipper1.removeViewAt(0);
			//			break;
			//		case R.id.next_weel_tv://下一个星期
			//			addGridView();
			//			currentWeek++;
			//			getCurrent();
			//			dateAdapter = new DateAdapter(this, getResources(), currentYear,
			//					currentMonth, currentWeek, currentNum, selectPostion,
			//					currentWeek == 1 ? true : false);
			//			dayNumbers = dateAdapter.getDayNumbers();
			//			gridView.setAdapter(dateAdapter);
			//			tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "年"
			//					+ dateAdapter.getCurrentMonth(selectPostion) + "月"
			//					+ dayNumbers[selectPostion] + "日");
			//			gvFlag++;
			//			flipper1.addView(gridView, gvFlag);
			//			dateAdapter.setSeclection(selectPostion);
			//			this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
			//					R.anim.push_left_in));
			//			this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
			//					R.anim.push_left_out));
			//			this.flipper1.showNext();
			//			flipper1.removeViewAt(0);
			//			break;
		case R.id.submit_shop_ok_bt:
			//发送数据
			if(show_choose_time_tv.getText().toString().isEmpty()){
				ToastUtil.makeShortText(this, "请选择到店时间");
				return;
			}
			//判断有没有添加收货人地址
			if ("点击添加收货地址".equals(people_name_tv.getText().toString())) {
				ToastUtil.makeShortText(this, "请添加收货人地址");
				return;
			}
			//			if(show_people_quantity_tv.getText().toString().isEmpty()){
			//				ToastUtil.makeShortText(this, "请填写到店人数");
			//				return;
			//			}
			//			if(people_name_et.getText().toString().isEmpty()){
			//				ToastUtil.makeShortText(this, "请填写您的姓名");
			//				return;
			//			}
			//			if(people_number_et.getText().toString().isEmpty()){
			//				ToastUtil.makeShortText(this, "请填写您的电话");
			//				return;
			//			}
			Intent intent = new Intent(this, EatShopReserve.class);
			intent.putExtra("title_location_tv", title_location_tv.getText());
			intent.putExtra("peopleName", people_name_tv.getText().toString());
			intent.putExtra("peopleNumber", number_tel_tv.getText().toString());
			intent.putExtra("show_choose_time_tv", show_choose_time_tv.getText());
			intent.putExtra("site_tv", site_tv.getText());
			//写死的三个数据最后是从后台获取的数据
			//			if(roomType==1){
			//				intent.putExtra("seattype", "散座4-50人桌");
			//			}else {
			//				intent.putExtra("seattype", "包间6-15人桌");
			//			}
			startActivity(intent);
			finish();
			break;
		}
	}
	/**
	 * 判断某年某月所有的星期数
	 * 
	 * @param year
	 * @param month
	 */
	public int getWeeksOfMonth(int year, int month) {
		// 先判断某月的第一天为星期几
		int preMonthRelax = 0;
		int dayFirst = getWhichDayOfWeek(year, month);
		int days = sc.getDaysOfMonth(sc.isLeapYear(year), month);
		if (dayFirst != 7) {
			preMonthRelax = dayFirst;
		}
		if ((days + preMonthRelax) % 7 == 0) {
			weeksOfMonth = (days + preMonthRelax) / 7;
		} else {
			weeksOfMonth = (days + preMonthRelax) / 7 + 1;
		}
		return weeksOfMonth;

	}

	/**
	 * 判断某年某月的第一天为星期几
	 * 
	 * @param year
	 * @param month
	 * @return
	 */
	public int getWhichDayOfWeek(int year, int month) {
		return sc.getWeekdayOfMonth(year, month);

	}

	/**
	 * 
	 * @param year
	 * @param month
	 */
	public int getLastDayOfWeek(int year, int month) {
		return sc.getWeekDayOfLastMonth(year, month,
				sc.getDaysOfMonth(isLeapyear, month));
	}

	public void getCalendar(int year, int month) {
		isLeapyear = sc.isLeapYear(year); // 是否为闰年
		daysOfMonth = sc.getDaysOfMonth(isLeapyear, month); // 某月的总天数
		dayOfWeek = sc.getWeekdayOfMonth(year, month); // 某月第一天为星期几
	}

	public int getWeeksOfMonth() {
		// getCalendar(year, month);
		int preMonthRelax = 0;
		if (dayOfWeek != 7) {
			preMonthRelax = dayOfWeek;
		}
		if ((daysOfMonth + preMonthRelax) % 7 == 0) {
			weeksOfMonth = (daysOfMonth + preMonthRelax) / 7;
		} else {
			weeksOfMonth = (daysOfMonth + preMonthRelax) / 7 + 1;
		}
		return weeksOfMonth;
	}

	private void addGridView() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		gridView = new GridView(this);
		gridView.setNumColumns(7);
		gridView.setGravity(Gravity.CENTER_VERTICAL);
		gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
		gridView.setVerticalSpacing(1);
		gridView.setHorizontalSpacing(1);
		gridView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return EatShopCustomTableActivity.this.gestureDetector.onTouchEvent(event);
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.i(TAG, "day:" + dayNumbers[position]);
				selectPostion = position;
				dateAdapter.setSeclection(position);
				dateAdapter.notifyDataSetChanged();
				tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "年"
						+ dateAdapter.getCurrentMonth(selectPostion) + "月"
						+ dayNumbers[position] + "日");
			}
		});
		gridView.setLayoutParams(params);
	}

	@Override
	protected void onPause() {
		super.onPause();
		jumpWeek = 0;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {

	}

	/**
	 * 重新计算当前的年月
	 */
	public void getCurrent() {
		if (currentWeek > currentNum) {
			if (currentMonth + 1 <= 12) {
				currentMonth++;
			} else {
				currentMonth = 1;
				currentYear++;
			}
			currentWeek = 1;
			currentNum = getWeeksOfMonth(currentYear, currentMonth);
		} else if (currentWeek == currentNum) {
			if (getLastDayOfWeek(currentYear, currentMonth) == 6) {
			} else {
				if (currentMonth + 1 <= 12) {
					currentMonth++;
				} else {
					currentMonth = 1;
					currentYear++;
				}
				currentWeek = 1;
				currentNum = getWeeksOfMonth(currentYear, currentMonth);
			}

		} else if (currentWeek < 1) {
			if (currentMonth - 1 >= 1) {
				currentMonth--;
			} else {
				currentMonth = 12;
				currentYear--;
			}
			currentNum = getWeeksOfMonth(currentYear, currentMonth);
			currentWeek = currentNum - 1;
		}
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		int gvFlag = 0;
		if (e1.getX() - e2.getX() > 80) {
			// 向左滑
			addGridView();
			currentWeek++;
			getCurrent();
			dateAdapter = new DateAdapter(this, getResources(), currentYear,
					currentMonth, currentWeek, currentNum, selectPostion,
					currentWeek == 1 ? true : false);
			dayNumbers = dateAdapter.getDayNumbers();
			gridView.setAdapter(dateAdapter);
			tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "年"
					+ dateAdapter.getCurrentMonth(selectPostion) + "月"
					+ dayNumbers[selectPostion] + "日");
			gvFlag++;
			flipper1.addView(gridView, gvFlag);
			dateAdapter.setSeclection(selectPostion);
			this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_in));
			this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_out));
			this.flipper1.showNext();
			flipper1.removeViewAt(0);
			return true;

		} else if (e1.getX() - e2.getX() < -80) {
			addGridView();
			currentWeek--;
			getCurrent();
			dateAdapter = new DateAdapter(this, getResources(), currentYear,
					currentMonth, currentWeek, currentNum, selectPostion,
					currentWeek == 1 ? true : false);
			dayNumbers = dateAdapter.getDayNumbers();
			gridView.setAdapter(dateAdapter);
			tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "年"
					+ dateAdapter.getCurrentMonth(selectPostion) + "月"
					+ dayNumbers[selectPostion] + "日");
			gvFlag++;
			flipper1.addView(gridView, gvFlag);
			dateAdapter.setSeclection(selectPostion);
			this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_in));
			this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_out));
			this.flipper1.showPrevious();
			flipper1.removeViewAt(0);
			return true;
		}
		return false;
	}
	//选择地址回调
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 101:
			Bundle b = data.getBundleExtra("site");
			Site site=(Site)b.getSerializable("site");
			//收货人电话
			number_tel_tv.setText(site.getSiteTel());
			//收货人姓名
			people_name_tv.setText(site.getSitePeopleName());
			//收货人地址
			site_tv.setText(site.getSite());
			break;
		}
	}
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return this.gestureDetector.onTouchEvent(event);
	}
	//	/**
	//	 * 选择发布的类型
	//	 */
	//	private void showChoiseDialog() {
	//		if (pw != null && pw.isShowing()) {
	//			pw.dismiss();
	//		} else {
	//			final View view = LayoutInflater.from(EatShopCustomTableActivity.this).inflate(
	//					R.layout.layout_eat_choose_time, null);
	//			//得带弹出对话框的对象
	//			pw = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT,
	//					ViewGroup.LayoutParams.MATCH_PARENT);
	//			ListView time_lv = (ListView)view.findViewById(R.id.time_lv);
	//			final ArrayList<String> typeAll = new ArrayList<String>();
	//			typeAll.add("9:00");typeAll.add("9:30");typeAll.add("10:00");typeAll.add("10:30");
	//			typeAll.add("11:00");typeAll.add("11:30");typeAll.add("12:00");typeAll.add("12:30");
	//			typeAll.add("13:00");typeAll.add("13:30");typeAll.add("14:00");typeAll.add("14:30");
	//			typeAll.add("15:00");typeAll.add("15:30");typeAll.add("16:00");typeAll.add("16:30");
	//			typeAll.add("17:00");typeAll.add("17:30");typeAll.add("18:00");typeAll.add("18:30");
	//			typeAll.add("19:00");typeAll.add("19:30");typeAll.add("20:00");typeAll.add("20:30");
	//			typeAll.add("21:00");
	//			time_lv.setAdapter(new TimeChooseAdapter(typeAll, EatShopCustomTableActivity.this));
	//			time_lv.setOnItemClickListener(new OnItemClickListener() {
	//
	//
	//				@Override
	//				public void onItemClick(AdapterView<?> arg0, View arg1,
	//						int arg2, long arg3) {
	//					time=tvDate.getText()+"  "+typeAll.get(arg2);
	//					String s = time.substring(time.indexOf("年")+1, time.length());
	//					show_choose_time_tv.setText(s);
	//					pw.dismiss();
	//				}
	//			});
	//			/**
	//			 * 这里设置显示PopuWindow之后在外面点击是否有效。
	//			 * 如果为false的话，那么点击PopuWindow外面并不会关闭PopuWindow。
	//			 * 当然这里很明显只能在Touchable下才能使用。
	//			 */
	//			pw.setOutsideTouchable(true);
	//			/**
	//			 * 当有mPop.setFocusable(false);的时候，
	//			 * 说明PopuWindow不能获得焦点，即使设置设置了背景不为空也不能点击外面消失，
	//			 * 只能由dismiss()消失，但是外面的View的事件还是可以触发,back键也可以顺利dismiss掉。
	//			 * 当设置为popuWindow.setFocusable(true);的时候，加上下面两行设置背景代码，点击外面和Back键才会消失。
	//				mPop.setFocusable(true);
	//				需要顺利让PopUpWindow dimiss（即点击PopuWindow之外的地方此或者back键PopuWindow会消失）；
	//				PopUpWindow的背景不能为空。
	//				必须在popuWindow.showAsDropDown(v);
	//				或者其它的显示PopuWindow方法之前设置它的背景不为空：
	//			 */
	//			pw.setFocusable(true);
	//			//点击外部关闭对话框
	//			view.setOnTouchListener(new OnTouchListener() {
	//				@Override
	//				public boolean onTouch(View v, MotionEvent event) {
	//					int with = view.findViewById(R.id.time_choose_ll).getLeft();
	//					int x = (int) event.getX();
	//					if (x < with) {
	//						pw.dismiss();
	//					}
	//					return false;
	//				}
	//			});
	//			// 此行代码可以在返回键按下的时候,使pw消失.(必须给背景)
	//			pw.setBackgroundDrawable(new BitmapDrawable());
	//			//设置动画样式
	//			pw.setAnimationStyle(R.style.AnimBottom);
	//			pw.showAtLocation(EatShopCustomTableActivity.this.getWindow().getDecorView(),
	//					Gravity.BOTTOM, 0, 0);
	//		}
	//	}


	/**-------------------日期选择器---------------------------------
	 * 初始化popupWindow
	 * @param view
	 */
	private void showPopwindow(View view) {
		menuWindow = new PopupWindow(view,LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		menuWindow.setFocusable(true);
		menuWindow.setBackgroundDrawable(new BitmapDrawable());
		menuWindow.showAtLocation(view, Gravity.CENTER_HORIZONTAL, 0, 0);
		menuWindow.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {
				menuWindow=null;
			}
		});
	}

	/**
	 * 
	 * @return
	 */
	private View getTimePick() {
		View view = inflater.inflate(R.layout.timepick, null);
		hour = (WheelView) view.findViewById(R.id.hour);
		hour.setAdapter(new NumericWheelAdapter(00, 23));
		//		hour.setLabel("时");
		hour.setCyclic(true);
		mins = (WheelView) view.findViewById(R.id.mins);
		mins.setAdapter(new NumericWheelAdapter(00, 59));
		//		mins.setLabel("分");
		mins.setCyclic(true);

		hour.setCurrentItem(0);
		mins.setCurrentItem(0);

		Button bt = (Button) view.findViewById(R.id.set);
		bt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					String HHstring;
					String mmString;
					if ((hour.getCurrentItem()+"").length()<2) {
						HHstring="0"+hour.getCurrentItem();
					}else {
						HHstring=hour.getCurrentItem()+"";
					}
					if ((mins.getCurrentItem()+"").length()<2) {
						mmString="0"+mins.getCurrentItem();
					}else {
						mmString=""+mins.getCurrentItem();
					}
					//选择的时间
					String str = HHstring + ":"+ mmString;
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm");
					String date = sdf.format(new Date());
					Date dateme = sdf1.parse(date+" "+str);
					System.out.println("选择的时间是="+date+" "+str+"     转化成毫秒值是="+dateme.getTime());
					//现在的时间
					String nowdate=sdf1.format(new Date());
					Date datenow = sdf1.parse(nowdate);
					System.out.println("当前的时间是="+nowdate+"     转化成毫秒值是="+datenow.getTime());
					System.out.println("选择时间和当前时间差=="+(dateme.getTime()-datenow.getTime()));
					if (dateme.getTime()-datenow.getTime()>=1200000) {
						show_choose_time_tv.setText(date+" "+str);
						menuWindow.dismiss();
					}else {
						ToastUtil.makeLongText(EatShopCustomTableActivity.this, "请选择20分钟后的时间");
					}
				} catch (Exception e) {
					System.out.println("时间解析出错");
					e.printStackTrace();
					menuWindow.dismiss();
				}
			}
		});
		Button cancel = (Button) view.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				menuWindow.dismiss();
			}
		});
		return view;
	}


	OnWheelScrollListener scrollListener = new OnWheelScrollListener() {

		@Override
		public void onScrollingStarted(WheelView wheel) {

		}

		@Override
		public void onScrollingFinished(WheelView wheel) {
			// TODO Auto-generated method stub
			int n_year = year.getCurrentItem() + 1950;// 楠烇拷
			int n_month = month.getCurrentItem() + 1;// 閺堬拷
			initDay(n_year,n_month);
		}
	};

	/**
	 * 
	 * @param year
	 * @param month
	 * @return
	 */
	private int getDay(int year, int month) {
		int day = 30;
		boolean flag = false;
		switch (year % 4) {
		case 0:
			flag = true;
			break;
		default:
			flag = false;
			break;
		}
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			day = flag ? 29 : 28;
			break;
		default:
			day = 30;
			break;
		}
		return day;
	}

	/**
	 */
	private void initDay(int arg1, int arg2) {
		day.setAdapter(new NumericWheelAdapter(1, getDay(arg1, arg2), "%02d"));
	}
	//-------------------日期选择器---------------------------------
}
