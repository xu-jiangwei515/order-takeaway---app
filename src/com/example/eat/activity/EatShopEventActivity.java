package com.example.eat.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.location.GpsStatus.Listener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.eat.adapter.EatShopEventAdapter;
import com.example.entity.ShopEvent;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.BaseActivity;

public class EatShopEventActivity extends BaseActivity{

	private ListView shop_event_lv;
	private TextView title_location_tv;
	private ImageView title_toreight_iv;
	private ImageView title_reight_iv;
	private RelativeLayout title_return_rl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_character);
		findView();
		init();
		Listener();
	}

	private void findView() {
		shop_event_lv=(ListView)findViewById(R.id.shop_event_lv);
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		
	}

	private void init() {
		title_location_tv.setText("优惠活动");
		title_toreight_iv.setVisibility(View.GONE);
		title_reight_iv.setVisibility(View.GONE);
		//通过网络获取活动内容
		ArrayList<ShopEvent> list = new ArrayList<ShopEvent>();
		for (int i = 0; i < 15; i++) {
			ShopEvent se = new ShopEvent();
			se.setEatName("红烧鱼");
			se.setEventImag("http://img2.imgtn.bdimg.com/it/u=930950245,1988316235&fm=21&gp=0.jpg");
			se.setRiginalPrice("5888");
			se.setGoingPrice("1888");
			list.add(se);
		}
		EatShopEventAdapter adapter = new EatShopEventAdapter(EatShopEventActivity.this, list);
		shop_event_lv.setAdapter(adapter);
	}

	private void Listener() {
		shop_event_lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent in=new Intent(EatShopEventActivity.this, EatChooseFoodActivity.class);
				startActivity(in);
				finish();
			}
		});
		title_return_rl.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
	}
	
}
