package com.example.eat.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dao.MyApplication;
import com.example.eat.adapter.EatChooseFoodAdapter;
import com.example.eat.adapter.EatChooseFoodExamineAdapter;
import com.example.entity.OrderPeople;
import com.example.myView.NoScrollListView;
import com.example.seeksubscribe.R;
import com.example.seeksubscribe.Activity.BaseActivity;
/**
 * 确认订单
 * @author User
 *
 */
public class EatShopReserve extends BaseActivity implements OnClickListener{
	private TextView title_location_tv;
	private ImageView title_reight_iv;
	private ImageView title_toreight_iv;
	private RelativeLayout title_return_rl;
	private TextView shop_name_tv;
	//	private TextView zwxx_tv;
	private TextView choose_time_tv;
	private TextView people_name_tv;
	private TextView number;
	private EditText beizhu;
	//	private RadioButton mounery_rb;
	private TextView mounery_tv;
	private Button submit_shop_ok_bt;
	private Button alter_eat_bt;
	private MyApplication myapp;
	
	private String title_location;
	private String show_choose_time;
	private String peoplenumber;
	private String peoplename;
	private NoScrollListView eat_seek_lv;
	private EatChooseFoodAdapter ECFadapter;
	private String site;
	private TextView address_site_tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(1);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eat_shop_custom_payment);
		myapp=MyApplication.getInstance();
		findview();
		initdata();
		listener();
	}

	private void findview() {
		//标题文字
		title_location_tv=(TextView)findViewById(R.id.title_location_tv);
		//从右往左第1个图标（收藏）
		title_reight_iv=(ImageView)findViewById(R.id.title_reight_iv);
		//从右往左第2个图标（分享）
		title_toreight_iv=(ImageView)findViewById(R.id.title_toreight_iv);
		//返回
		title_return_rl=(RelativeLayout)findViewById(R.id.title_return_rl);
		//商店名字
		shop_name_tv=(TextView)findViewById(R.id.shop_name_tv);
		//订座类型
		//		zwxx_tv=(TextView)findViewById(R.id.zwxx_tv);
		//到店时间
		choose_time_tv=(TextView)findViewById(R.id.choose_time_tv);
		//订座人姓名
		people_name_tv=(TextView)findViewById(R.id.people_name_tv);
		//订座人电话
		number=(TextView)findViewById(R.id.number);
		//备注
		beizhu=(EditText)findViewById(R.id.beizhu);
		//确定交钱按钮
		//		mounery_rb=(RadioButton)findViewById(R.id.mounery_rb);
		//定金钱数
		mounery_tv=(TextView)findViewById(R.id.mounery_tv);
		//地址显示
		address_site_tv=(TextView)findViewById(R.id.address_site_tv);
		//确定按钮
		submit_shop_ok_bt=(Button)findViewById(R.id.submit_shop_ok_bt);
		//修改菜品按钮
		alter_eat_bt=(Button)findViewById(R.id.alter_eat_bt);
		//分类listView
		eat_seek_lv=(NoScrollListView)findViewById(R.id.eat_seek_lv);
		title_location_tv.setText("在线预订");
		title_reight_iv.setVisibility(View.GONE);
		title_toreight_iv.setVisibility(View.GONE);
	}

	private void initdata() {
		Intent intent = getIntent();
		//获取上个页面发过来的数据，显示
		title_location=intent.getStringExtra("title_location_tv");
		shop_name_tv.setText(title_location);
		//		zwxx_tv.setText(intent.getStringExtra("seattype"));
		show_choose_time=intent.getStringExtra("show_choose_time_tv");
		choose_time_tv.setText(show_choose_time);
		peoplenumber=intent.getStringExtra("peopleNumber");
		number.setText(peoplenumber);
		peoplename=intent.getStringExtra("peopleName");
		people_name_tv.setText(peoplename);
		mounery_tv.setText(myapp.sun_money);
		site=intent.getStringExtra("site_tv");
		address_site_tv.setText(site);
		//修改数据
		EatChooseFoodExamineAdapter ecfeAdapter = new EatChooseFoodExamineAdapter(EatShopReserve.this);
		eat_seek_lv.setAdapter(ecfeAdapter);
	}

	private void listener() {
		mounery_tv.setOnClickListener(this);
		title_return_rl.setOnClickListener(this);
		alter_eat_bt.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_return_rl:
			finish();
			break;
		case R.id.submit_shop_ok_bt:
			//调用支付宝API
			break;
		case R.id.alter_eat_bt:
			//修改菜品
			Intent i = new Intent(EatShopReserve.this, EatChooseFoodActivity.class);
			myapp.mark=1;
			i.putExtra("title_location_tv", title_location);
			i.putExtra("peopleName", peoplename);
			i.putExtra("peopleNumber", peoplenumber);
			i.putExtra("show_choose_time_tv", show_choose_time);
			i.putExtra("site_tv", site);
			startActivity(i);
			finish();
			break;
		}

	}
}
