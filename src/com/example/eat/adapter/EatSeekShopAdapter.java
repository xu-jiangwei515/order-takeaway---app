package com.example.eat.adapter;

import java.util.ArrayList;

import com.example.entity.ShopLv;
import com.example.seeksubscribe.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EatSeekShopAdapter extends BaseAdapter{


	private ArrayList<ShopLv> list;
	private LayoutInflater inflater;

	public EatSeekShopAdapter(ArrayList<ShopLv> list,Context context) {
		this.list=list;
		inflater=LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		ViewHander hander=null;
		if(contentView==null){
			contentView=inflater.inflate(R.layout.item_eat_seek, null);
			hander=new ViewHander();
			hander.pathPicture=(ImageView) contentView.findViewById(R.id.shop_pathPicture_iv);
			hander.shopName=(TextView) contentView.findViewById(R.id.shop_Name_tv);
			hander.shopType=(TextView) contentView.findViewById(R.id.shop_type_tv);
			hander.shopMoney=(TextView) contentView.findViewById(R.id.eat_each_average);
			hander.shopRange=(TextView) contentView.findViewById(R.id.shop_Range_tv);
			contentView.setTag(hander);
		}else {
			hander=(ViewHander) contentView.getTag();
		}
		//图片通过网络获取Imageloader
		hander.shopName.setText(list.get(position).getShopName());
		hander.shopType.setText(list.get(position).getShopType());
		hander.shopMoney.setText(list.get(position).getShopMoney());
		hander.shopRange.setText(list.get(position).getShopRange());
		return contentView;
	}
	class ViewHander{
		ImageView pathPicture;
		TextView shopName;
		TextView shopType;
		TextView shopMoney;
		TextView shopRange;
	}
}
