package com.example.eat.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.test.TouchUtils;
import android.text.method.Touch;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dao.MyApplication;
import com.example.eat.activity.EatChooseFoodActivity;
import com.example.eat.activity.EatShopReserve;
import com.example.entity.Dishes;
import com.example.seeksubscribe.R;
import com.example.utils.ToastUtil;
import com.example.utils.Utils;



public class EatChooseFoodExamineAdapter extends BaseAdapter{

	private LayoutInflater inflater;
	private Context context;
	private MyApplication myapp;
	private static int totalmoney=0;

	public EatChooseFoodExamineAdapter(Context Context){
		this.context=Context;
		inflater=LayoutInflater.from(Context);
		myapp=MyApplication.getInstance();
	}
	@Override
	public int getCount() {
		return myapp.ChooseList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return myapp.ChooseList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHander holder=null;
		if(convertView==null){
			convertView=inflater.inflate(R.layout.item_eat_choose_food_examine, null);
			holder=new ViewHander();
			holder.shop_Name_tv=(TextView)convertView.findViewById(R.id.shop_Name_tv);
			holder.eat_xianjia_tv=(TextView)convertView.findViewById(R.id.eat_xianjia_tv);
			holder.yuanjia_tv=(TextView)convertView.findViewById(R.id.yuanjia_tv);
			holder.number_tv=(TextView)convertView.findViewById(R.id.number_tv);
			holder.shop_pathPicture_iv=(ImageView)convertView.findViewById(R.id.shop_pathPicture1_iv);
			convertView.setTag(holder);
		}else {
			holder=(ViewHander) convertView.getTag();
		}
		holder.shop_Name_tv.setText(myapp.ChooseList.get(position).getName());
		holder.eat_xianjia_tv.setText(myapp.ChooseList.get(position).getCurrentCost());
		holder.yuanjia_tv.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG);
		holder.yuanjia_tv.setText(myapp.ChooseList.get(position).getOriginalCost());
		holder.number_tv.setText(myapp.ChooseList.get(position).getNumber()+"");
		//获取图片路径后设置显示图片ImageLorder
//		holder.shop_pathPicture_iv.setText(list.get(position).getName());
		return convertView;
	}
	class ViewHander{
		TextView shop_Name_tv;
		TextView eat_xianjia_tv;
		TextView yuanjia_tv;
		TextView number_tv;
		//菜品图片
		ImageView shop_pathPicture_iv;

	}

}
