package com.example.eat.adapter;

import java.util.ArrayList;

import com.example.entity.ShopLv;
import com.example.seeksubscribe.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EatSortTypeToAdapter extends BaseAdapter{


	private ArrayList<String> list;
	private LayoutInflater inflater;

	public EatSortTypeToAdapter(Context context) {
		inflater=LayoutInflater.from(context);
	}
	public void setData(ArrayList<String> typeAll){
		this.list=typeAll;
	}
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent) {
		ViewHander hander=null;
		if(contentView==null){
			contentView=inflater.inflate(R.layout.item_eat_all_type_to, null);
			hander=new ViewHander();
			hander.type_tv=(TextView) contentView.findViewById(R.id.type_to_tv);
			contentView.setTag(hander);
		}else {
			hander=(ViewHander) contentView.getTag();
		}
		hander.type_tv.setText(list.get(position));
		return contentView;
	}
	class ViewHander{
		TextView type_tv;
	}
}
