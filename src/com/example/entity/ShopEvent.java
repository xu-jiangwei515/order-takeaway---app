package com.example.entity;

import java.io.Serializable;

public class ShopEvent implements Serializable{
	String shipId;
	String eventImag;
	String eatName;
	String  riginalPrice;
	String goingPrice;
	public String getShipId() {
		return shipId;
	}
	public void setShipId(String shipId) {
		this.shipId = shipId;
	}
	public String getEventImag() {
		return eventImag;
	}
	public void setEventImag(String eventImag) {
		this.eventImag = eventImag;
	}
	public String getEatName() {
		return eatName;
	}
	public void setEatName(String eatName) {
		this.eatName = eatName;
	}
	public String getRiginalPrice() {
		return riginalPrice;
	}
	public void setRiginalPrice(String riginalPrice) {
		this.riginalPrice = riginalPrice;
	}
	public String getGoingPrice() {
		return goingPrice;
	}
	public void setGoingPrice(String goingPrice) {
		this.goingPrice = goingPrice;
	}
	@Override
	public String toString() {
		return "ShopEvent [shipId=" + shipId + ", eventImag=" + eventImag
				+ ", eatName=" + eatName + ", riginalPrice=" + riginalPrice
				+ ", goingPrice=" + goingPrice + "]";
	}
	
	
}
